import ctypes
from enum import IntEnum, IntFlag

# libudspt = ctypes.cdll.LoadLibrary("/virgoDev/libudspt/v1r1/Linux-x86_64-CL7/libudspt.so")
#libudspt = ctypes.cdll.LoadLibrary("/virgoDev/libudspt/v1r2/Linux-x86_64-CL7/libudspt.so")
libudspt = ctypes.cdll.LoadLibrary("/virgoDev/libudspt/v1r1p3/Linux-x86_64-CL7/libudspt.so")

DspHandler = ctypes.POINTER(ctypes.c_char)
new_dsp = libudspt.new_dsp
new_dsp.argtypes = [ctypes.c_char_p]
new_dsp.restype = DspHandler

delete_dsp = libudspt.delete_dsp
delete_dsp.argtypes = [DspHandler]
delete_dsp.restype = None

get_dsp_status = libudspt.get_dsp_status
get_dsp_status.argtypes = [DspHandler, ctypes.POINTER(ctypes.c_char_p),
                           ctypes.POINTER(ctypes.c_int)]
get_dsp_status.restype = ctypes.c_int

download = libudspt.download
download.argtypes = [DspHandler, ctypes.c_char_p]
download.restype = ctypes.c_int

damping_download = libudspt.damping_download
damping_download.argypes = [DspHandler, ctypes.c_char_p, ctypes.c_int]
damping_download.restype = ctypes.c_int

send_command = libudspt.send_command
send_command.argtypes = [DspHandler, ctypes.c_char_p, ctypes.c_int]
send_command.restype = ctypes.c_int

get_variable = libudspt.get_variable
get_variable.argtypes = [DspHandler, ctypes.c_char_p, ctypes.c_int, ctypes.POINTER(ctypes.c_double)]
get_variable.restype = ctypes.c_int

get_gain = libudspt.get_gain
get_gain.argtypes = [DspHandler, ctypes.c_char_p, ctypes.c_ushort,
                              ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double)]
get_gain.restype = ctypes.c_int

# get_variable_list

set_gain = libudspt.set_gain
set_gain.argtypes = [DspHandler, ctypes.c_char_p, ctypes.c_double, ctypes.c_ushort, ctypes.c_double]
set_gain.restype = ctypes.c_int

set_gain_from_address = libudspt.set_gain_from_address
libudspt.set_gain_from_address.argtypes = [DspHandler, ctypes.c_uint, ctypes.c_int, ctypes.c_double]
libudspt.set_gain_from_address.restype = ctypes.c_int

get_gain_list = libudspt.get_gain_list
get_gain_list.argtypes = [DspHandler, ctypes.c_int, ctypes.POINTER(ctypes.c_char_p),
                          ctypes.POINTER(ctypes.c_char_p), ctypes.POINTER(ctypes.c_int)]
get_gain_list.restype = ctypes.c_int

reset_filter = libudspt.reset_filter
reset_filter.argtypes = [DspHandler, ctypes.c_char_p, ctypes.c_int]
reset_filter.restype = ctypes.c_int

get_frequency = libudspt.get_frequency
get_frequency.argtypes = [DspHandler, ctypes.c_int, ctypes.POINTER(ctypes.c_double)]
get_frequency.restype = ctypes.c_int

get_uptime = libudspt.get_uptime
get_uptime.argtypes = [DspHandler, ctypes.c_char_p]
get_uptime.restype = ctypes.c_int

get_gps_time = libudspt.get_gps_time
get_gps_time.argtypes = [DspHandler, ctypes.POINTER(ctypes.c_double)]
get_gps_time.restype = ctypes.c_int

get_temperature = libudspt.get_temperature
get_temperature.argtypes = [DspHandler, ctypes.POINTER(ctypes.c_float)]
get_temperature.restype = ctypes.c_int

get_fw_version = libudspt.get_fw_version
get_fw_version.argtypes = [DspHandler, ctypes.c_char_p]
get_fw_version.restype = ctypes.c_int

get_sw_version = libudspt.get_sw_version
get_sw_version.argtypes = [DspHandler, ctypes.c_char_p]
get_sw_version.restype = ctypes.c_int

get_mac_and_slot = libudspt.get_mac_and_slot
get_mac_and_slot.argtype = [DspHandler, ctypes.c_char_p, ctypes.POINTER(ctypes.c_ushort)]


class Udspt:
    class DspStatus(IntEnum):
        EXIT_DSP_SOCKETNOTOPENED = -1
        EXIT_DSP_SOCKETNOTREADY	= -2
        EXIT_DSP_SOCKETGENERICERROR = -3
        EXIT_DSP_FILENOTOPEN = -4
        EXIT_DSP_DNS_FAILURE = -5
        # EXIT_DSP_INVALIDFILENAME = None #None because function dmfopen returns a pointer
        EXIT_DSP_INVALIDIP = -6
        EXIT_DSP_UNABLESENDMSG	= -7
        EXIT_DSP_ACKERROR = -8
        EXIT_DSP_NOTCONNECTED =	-9
        EXIT_DSP_GETCHERROR1 = -10
        EXIT_DSP_GETCHERROR2 = -11
        EXIT_STATUS_DSP_UNKNOWN = -12
        EXIT_DSP_INVALIDVARIABLE = -13
        EXIT_DSP_GENERICERROR = -14
        EXIT_DSP_INVALIDCORE  = -15
        EXIT_DSP_UNDEFINED = -16
        EXIT_DSP_CONFIGERROR = -17
        # Good values
        EXIT_DSP_OK = 0

        STATUS_DSP_RUNNING = 0
        STATUS_DSP_IDLE	= 1
        STATUS_DSP_HALTED = 2
        STATUS_DSP_KILLED = 3

    class DspFlags(IntFlag):
        OK = 0x0
        STATUS_ERROR = 0x1
        FILTER_ERROR = 0x2
        TEMPERATURE_WARN = 0x4
        TEMPERATURE_ERROR = 0x8
        GPS_TIME_ERROR = 0x10
        FREQUENCY_ERROR = 0x20
        SW_VERSION_ERROR = 0x40
        FW_VERSION_ERROR = 0x80
        NO_CONFIG_ENTRY = 0x100


    def __init__(self, ip_addr, serial_number=None, tower=None, chassis=None):
        self.ip_addr = ip_addr
        self.serial_number = serial_number
        self.tower = tower
        self.chassis = chassis
        self.dsp = new_dsp(bytes(ip_addr, 'utf-8'))
        self.flags = Udspt.DspFlags.OK
        self.software = []
        self.running_filter = []
        self.status = None
        self.defaults = {}

    def __del__(self):
        delete_dsp(self.dsp)


    def set_status_flags(self, status):
        self.status = Udspt.DspStatus(status)
        if self.status >= Udspt.DspStatus.EXIT_DSP_OK:
            self.flags = self.flags & ~Udspt.DspFlags.STATUS_ERROR
        else:
            self.flags = self.flags | Udspt.DspFlags.STATUS_ERROR


    def get_dsp_status(self):
        max_size = 15
        keys = (ctypes.c_char_p * max_size)()
        size = ctypes.c_int(max_size)
        self.set_status_flags(get_dsp_status(self.dsp, keys, ctypes.byref(size)))
        if self.status >= self.DspStatus.EXIT_DSP_OK:
            self.software = [k.decode('utf-8') for k in keys if k is not None]
            self.running_filter = self.software[1]
        return self.status


    def download(self, filename):
        s = download(self.dsp, bytes(filename, 'utf-8'))
        self.set_status_flags(s)
        # Add some stuff from DspServer.cpp


    def damping_download(self, filename, core=4):
        s = damping_download(self.dsp, filename, ctypes.c_int(core))
        self.set_status_flags(s)

    def send_command(self, command, core=4):
        s = send_command(self.dsp, command.encode('utf-8'), ctypes.c_int(core))
        self.set_status_flags(s)

    #
    def get_variable(self, var, core=4):
        value = ctypes.c_double()
        s = get_variable(self.dsp, var.encode('utf-8'), ctypes.c_int(core), ctypes.byref(value))
        self.set_status_flags(s)
        if self.status >= self.DspStatus.EXIT_DSP_OK:
            return value.value
        return float('nan')


    #
    def get_gain(self, gain, core=4):
        value = ctypes.c_double()
        last_value = ctypes.c_double()
        s = get_gain(self.dsp, bytes(gain, 'utf-8'), core, ctypes.byref(value),
                     ctypes.byref(last_value))
        self.set_status_flags(s)
        if self.status >= self.DspStatus.EXIT_DSP_OK:
            return value.value
        return float('nan')

    #
    def set_gain(self, gain, value, core=4, ramp_time=0):
        s = set_gain(self.dsp, bytes(gain, 'utf-8'), ctypes.c_double(value), core, ramp_time)
        self.status = self.DspStatus(s)
        return self.status


    def set_gain_from_address(self, addr, core, value):
        s = set_gain_from_address(self.dsp, ctypes.c_uint(addr), ctypes.c_int(core),
                                  ctypes.c_double(value))
        self.set_status_flags(s)
        return self.status


    #
    def get_fw_version(self):
        fw_version_buffer = ctypes.create_string_buffer(50)
        s = get_fw_version(self.dsp, fw_version_buffer)
        self.set_status_flags(s)
        return fw_version_buffer.value.decode('utf-8')

    # Get software version
    def get_sw_version(self):
        sw_version_buffer = ctypes.create_string_buffer(50)
        s = get_sw_version(self.dsp, sw_version_buffer)
        self.set_status_flags(s)
        return sw_version_buffer.value.decode('utf-8')


    # Get uptime
    def get_uptime(self):
        uptime_buffer = ctypes.create_string_buffer(50)
        s = get_uptime(self.dsp, uptime_buffer)
        self.status = self.DspStatus(s)
        del uptime_buffer
        if self.status >= self.DspStatus.EXIT_DSP_OK:
            return uptime_buffer.value.decode('utf-8')
        return float('nan')


    # Get gps time
    def get_gps_time(self):
        gps_sec = ctypes.c_double()
        self.set_status_flags(get_gps_time(self.dsp, ctypes.byref(gps_sec)))
        if self.status >= self.DspStatus.EXIT_DSP_OK:
            return gps_sec.value
        return float('nan')


    # Get temperature
    def get_temperature(self, ntemp=3):
        temperature_buffer = (ctypes.c_float * ntemp)()  # Assuming you want to retrieve 3 temperature values
        self.set_status_flags(get_temperature(self.dsp, temperature_buffer))
        temperature_list = [temperature_buffer[i] for i in range(ntemp)]
        return temperature_list

    def get_frequency(self, core):
        frequency = ctypes.c_double()
        s = get_frequency(self.dsp, ctypes.c_int(core), ctypes.byref(frequency))
        self.set_status_flags(s)
        return frequency.value

    def get_gain_list(self, core):
        max_size = 128  # Adjust the size as needed

        # Allocate arrays for keys and values
        keys = (ctypes.c_char_p * max_size)()
        values = (ctypes.c_char_p * max_size)()
        size = ctypes.c_int(max_size)

        s = get_gain_list(self.dsp, ctypes.c_int(core), keys, values, ctypes.byref(size))
        self.set_status_flags(s)

        res = {keys[i].decode('utf-8'): values[i].decode('utf-8')
               for i in range(size.value)}
        return res


    def reset_filter(self, filter_name, core):
        s = reset_filter(bytes(filter_name, 'utf-8'), ctypes.c_int(core))
        self.set_status_flags(s)
        return self.status


    def get_mac_and_slot(self):
        mac_buffer = ctypes.create_string_buffer(50)
        slot = ctypes.c_ushort()
        s = get_mac_and_slot(self.dsp, mac_buffer, ctypes.byref(slot))
        self.set_status_flags(s)
        if self.status >= Udspt.DspStatus.EXIT_DSP_OK:
            return mac_buffer.value.decode('utf-8'), slot.value
        return '00:00:00:00:00:00', int('nan')
