#!/usr/bin/env python3

'''
DSP_sq module includes classes interfacing with Finite-State Machines implemented in UDSPTs.
'''

import math
import time
import pickle
import os.path
import getpass
import logging
import logging.handlers as logh
from DSP_sq.udspt import Udspt

__author__ = "Mateusz Bawaj"
__copyright__ = "Copyright 2019, Virgo Project"
__credits__ = ["Mateusz Bawaj, Diego Pasuello"]
__license__ = "CC0"
__version__ = "1.2.0"
__maintainer__ = "Mateusz Bawaj"
__email__ = "mateusz.bawaj@pg.infn.it"
__status__ = "Production"

# Enable logging
enable_logging = True
#log_file_path = '/virgoDev/Squeezing1500/SAT_controls/squeez_loop.log'
log_file_path = '/data/dev/logs/Squeezing1500/squeez_loop.log'

if enable_logging is True:
    #logging.basicConfig(filename=log_file_path)
    logger = logging.getLogger(__package__ + ":" + getpass.getuser())
    logger.setLevel(logging.INFO)

    # create a file handler
    try:
        handler = logh.WatchedFileHandler(log_file_path)
    except Exception as e:
        print("ERROR: Unable to open log file. Logging disabled!")
        enable_logging = False
    else:
        handler.setLevel(logging.INFO)

        # create a logging format
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(handler)
        
    # define a Handler which writes WARNING messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.WARNING)
    # add the handler to the logger
    logger.addHandler(console)

class DSP_device:
    """
    Class of devices in squeezing lab. It uses PyTango server \
    for communication with the UDSPT.
    enabled - (boolean) indicates if the loop is enabled. Can be used for automation \
    of the GUI
    _tango_device - It is automatically assigned with a PyTango.DeviceProxy class type \
    object by the contructor.
    _tango_device_name - Device name for device identification in Tango.
    _serial_number - Serial number of the board. It corresponds to the \
    serial number in the inventory database available under http://slwebtest3.virgo.infn.it/inventory/
    _dev_state - Some of the available states of the UDSPT are "RUNNING", "ALARM", "FAULT", ...
    _board_name - Board name given arbitrarily by the user.
    _core1_glist - List of gains from the first core which are mapped to \
    Tango in the current UDSPT software
    _core4_glist - List of gains from the fourth core which are mapped to \
    Tango in the current UDSPT software
    """
    tango_device = None
    _tango_device_name = ""
    _serial_number = ""
    _dev_state = ""
    _board_name = ""
    _core1_glist = None
    _core4_glist = None
    
    def __init__(self, tango_device_name):
        '''
        tango_device_name - String which identifies Tango device e.g. 'dspserver/p42/98'.
        '''
        self._tango_device_name = tango_device_name
        
        self._devs = {"squeeze1": "172.16.2.55", "squeeze2": "172.16.2.98", "squeeze3": "172.16.2.112"}
        self._board_name = tango_device_name
        
        try:
            #self.tango_device = PyTango.DeviceProxy(tango_device_name)
            self.tango_device = Udspt(self._devs[self._tango_device_name])
        except Exception as e:
            for item in e:
                logger.error("Error: " + item.desc)
            raise Exception('Device does not exist in Tango!')
        
        self._dev_state = str(self.tango_device.get_dsp_status())

    
    def _getGainList(self):
        '''
        Receives list of gains which are mapped to Tango in the current \
        UDSPT software.
        '''
        try:
            self._core1_glist = self.tango_device.get_gain_list(1)
            self._core4_glist = self.tango_device.get_gain_list(4)
        except Exception as e:
            logger.warning("User without permissions to DSP!")
            for item in e:
                logger.error("Error: " + item.desc)
            #raise Exception('GetGainList() command failed!')
        
    def printGainList(self):
        '''
        Prints out list of gains in the core 1 and core 4 if they are present.
        '''
        try:
            self._getGainList()
        except Exception as e:
            logger.warning("User without permissions to DSP!")
            for item in e:
                logger.error("Error: " + item.desc)
            #raise Exception('GetGainList() command failed!')
        else:
            if self._core1_glist:
                #print("Core 1 gains: " + str(self._core1_glist))
                logger.info("Core 1 gains: " + str(self._core1_glist))
            
            if self._core4_glist:
                #print("Core 4 gains: " + str(self._core4_glist))
                logger.info("Core 4 gains: " + str(self._core4_glist))
            
    def info(self):
        '''
        Prints out all information about the DSP tango device stored in\
        the DSP_sq_dev object.
        '''
        logger.info("Tango device : " + self._tango_device_name)
        logger.info("Serial number: " + self._serial_number)
        logger.info("Board name   : " + self._board_name)
        logger.info("Device state : " + self._dev_state)


class LoopSettings:
    '''
    Object of this class stores parameters of the loop.
    '''
    def __init__(self, name, gain, ramp_freq=1.0, ramp_ampl=0.0, \
                 ramp_offs=0.0, setpoint=0.0, enabled=False):
        self.name = name
        self.gain = gain
        self.setpoint = setpoint
        self.ramp_freq = ramp_freq
        self.ramp_ampl = ramp_ampl
        self.ramp_offs = ramp_offs
        self.enabled = enabled

    def print_settings(self):
        '''
        Prints out all information about the loop stored in\
        the LoopSettings object.
        '''
        logger.info("=== " + self.name + " loop parameters ===")
        logger.info("Loop gain: " + str(self.gain))
        logger.info("Setpoint : " + str(self.setpoint))
        logger.info("Ramp freq: " + str(self.ramp_freq))
        logger.info("Ramp ampl: " + str(self.ramp_ampl))
        logger.info("Ramp offs: " + str(self.ramp_offs))
        logger.info("Loop is " + ("enabled" if self.enabled else "disabled"))


class Loop:
    """Class of locking loops in squeezing lab 1500W."""
    
    __enabled = False
    __dev_proxy = None
    
    __predcessor = None
    __successor = None
    
    # Loop gnames and varnames
    _button_gname = ""
    _gain_gname = ""
    _status_varname = ""
    
    # Test ramp gnames
    _ramp_freq_gname = ""
    _ramp_ampl_gname = ""
    _ramp_offs_gname = ""
    
    # Signals varnames
    _err_signal = ""
    _tran_signal = ""
    
    # PID loop gnames
    _setpoint_gname = ""
    
    # Debug varnames
    _test_min = ""
    _test_max = ""
    
    param = None
    _settings_filename_suffix = "_set.pkl"

    def __init__(self, dsp_device, loop_name, button_gname, status_varname,\
                 gain_gname, ramp_freq_gname, ramp_ampl_gname, ramp_offs_gname,\
                 err_signal_varname, tran_signal_varname, setpoint_gname = "",\
                 test_min_varname = "", test_max_varname = "", predcessor = None,\
                 successor = None, filter_name = None):
        '''
        Minimum amount of information which must be given to the constructor:\
        dsp_device - object of DSP_device type which supply the target device\
        and names of all gnames/varnames which are in use in the loop.
        button_gname - Switch of state of the loop.
        status_varname - Number which identifies the state of the loop.
        gain_gname - Loop filter gain.
        Test ramp parameters. Test ramp is usually used for optical \
        alignment procedure.
        ramp_freq_gname - Test ramp frequency.
        ramp_ampl_gname - Test ramp amplitude.
        ramp_offs_gname - Test ramp offset.
        err_signal_varname - Slow error signal monitor.
        tran_signal_varname - Slow transition photodiode monitor.
        setpoint_gname - Setpoint (necessary for PID type loop).
        test_min_varname - Minimum value found during calibration.
        test_max_varname - Maximum value found during calibration.
        predcessor - Loop which must be enabled before performing lock attempt.
        successor - Loop which must be disabled before shutdown.
        '''
        self.__dsp_device = dsp_device
        self.__dev_proxy = self.__dsp_device.tango_device
        self._button_gname = button_gname
        self._status_varname = status_varname
        self._gain_gname = gain_gname
        
        self._ramp_freq_gname = ramp_freq_gname
        self._ramp_ampl_gname = ramp_ampl_gname
        self._ramp_offs_gname = ramp_offs_gname
        
        self._err_signal_varname = err_signal_varname
        self._tran_signal_varname = tran_signal_varname
        
        self._setpoint_gname = setpoint_gname
        self._test_min = test_min_varname
        self._test_max = test_max_varname
        
        self.param = LoopSettings(loop_name, 0.0)  # Empty Settings
        
        self.__predcessor = predcessor
        self.__successor = successor
        
        self._filter_name = filter_name


    def _read_var(self, core, varname):
        '''
        Reads variable of the name varname form the right core.
        '''
        try:
            #result = self.__dev_proxy.GetVariable([[int(core),],[varname,]])
            result = self.__dev_proxy.get_variable(varname, core=int(core))
        except Exception as e:
            logger.exception('')
        else:
            if math.isnan(result):
                raise Exception('Varname: "' + varname + '" does not exist')
            else:
                return result

    def _read_gain(self, core, gname):
        '''
        Reads gain of the name gname from the right core.
        '''
        try:
            #result = self.__dev_proxy.GetGain([[int(core),],[gname,]])
            result = self.__dev_proxy.get_gain(gname, core=int(core))
        except Exception as e:
            logger.exception('')
        else:
            if math.isnan(result):
                raise Exception('Gname: "' + gname + '" does not exist')
            else:
                return result
            
    def set_predcessor(self, predc):
        '''
        Sets predcessor field of the class.
        
        This function is used for automation. Predcessor field stores the refence\
        to the loop object which has to be engaged before this loop.
        '''
        self.__predcessor = predc
        
    def set_successor(self, succ):
        '''
        Sets succeccor field of the class.
        
        This function is used for automation. Predcessor field stores the refence\
        to the loop object which has to be engaged after this loop.
        '''
        self.__successor = succ
            
    def set_ramp_freq(self, freq_value):
        '''
        Changes test ramp frequency.
        '''
        #self.param.ramp_freq = freq_value
        #self.__dev_proxy.SetGain([[4, freq_value],[self._ramp_freq_gname]])
        self.__dev_proxy.set_gain(self._ramp_freq_gname, freq_value, core=4)
        
    def get_ramp_freq(self):
        '''
        Queries UDSPT for ramp frequency.
        '''
        try:
            temp = self._read_gain(4, self._ramp_freq_gname)
        except Exception as e:
            logger.warn(str(e) + " in " + self.param.name + '!')
            return 0.
        else:
        #self.param.ramp_freq = temp
            return temp
        
    def set_ramp_ampl(self, ampl_value):
        '''
        Changes test ramp amplitude.
        '''
        #self.param.ramp_ampl = ampl_value
        #self.__dev_proxy.SetGain([[4, ampl_value],[self._ramp_ampl_gname]])
        self.__dev_proxy.set_gain(self._ramp_ampl_gname, ampl_value, core=4)
        
    def get_ramp_ampl(self):
        '''
        Queries UDSPT for ramp amplitude.
        '''
        try:
            temp = self._read_gain(4, self._ramp_ampl_gname)
        except Exception as e:
            logger.warning(str(e) + " in " + self.param.name + '!')
            return 0.
        else:
        #self.param.ramp_ampl = tenp
            return temp
        
    def set_ramp_offs(self, offs_value):
        '''
        Changes test ramp offset.
        '''
        #self.param.ramp_offs = offs_value
        #self.__dev_proxy.SetGain([[4, offs_value],[self._ramp_offs_gname]])
        self.__dev_proxy.set_gain(self._ramp_offs_gname, offs_value, core=4)
        
    def get_ramp_offs(self):
        '''
        Queries UDSPT for ramp offset.
        '''
        try:
            temp = self._read_gain(4, self._ramp_offs_gname)
        except Exception as e:
            logger.warning(str(e) + " in " + self.param.name + '!')
            return 0.
        else:
        #self.param.ramp_offs = temp
            return temp
            
    def set_loop_gain(self, gain_value):
        '''
        Changes gain of the loop filter.
        '''
        #self.param.gain = gain_value
        #self.__dev_proxy.SetGain([[4, gain_value],[self._gain_gname]])
        self.__dev_proxy.set_gain(self._gain_gname, gain_value, core=4)
        
        if enable_logging is True:
            logger.info(self.param.name + ' gain set to ' + str(gain_value))
        
    def get_loop_gain(self):
        '''
        Reads gain of the loop filter. The function returns float value\
        or rises an exception if gname does not exist.
        '''
        try:
            temp = self._read_gain(4, self._gain_gname)
        except Exception as e:
            logger.warning(str(e) + " in " + self.param.name + '!')
            return 0.
        else:
        #self.param.gain = temp
            return temp
            
    def get_error_signal(self):
        '''
        Reads variable of the error signal. The function returns float value\
        or rises an exception if gname does not exist.
        '''
        return self._read_var(4, self._err_signal_varname)
        
    def get_tran_signal(self):
        '''
        Reads variable of the transition signal. The function returns float value\
        or rises an exception if gname does not exist.
        '''
        return self._read_var(4, self._tran_signal_varname)
    
    def check_status(self):
        '''
        Checks the status of the loop and return it as an int.
        
        Possible numeric return values with meanings:
        0 - unlocked
        1 - search for lock
        2 - locked
        3 - cavity alignment ramp
        13 - error
        The function rises an exception if gname does not exist.
        '''
        result = int(self._read_var(4, self._status_varname))
        #if result == 13:
        #    raise Exception('FSM in error state!')
        return result
        
    def set_setpoint(self, setpoint_value):
        '''
        Changes setpoint of the PID loop filter.
        '''
        #self.__dev_proxy.SetGain([[4, setpoint_value],[self._setpoint_gname]])
        self.__dev_proxy.set_gain(self._setpoint_gname, setpoint_value, core=4)
        
        if enable_logging is True:
            logger.info(self.param.name + ' setpoint set to ' + str(setpoint_value))
    
    def get_setpoint(self):
        '''
        Reads setpoint of the PID loop filter.
        '''
        return self._read_gain(4, self._setpoint_gname)
        
    def get_test_min(self):
        '''
        Queries UDSPT for the calibration data and return minimun of transmission.
        '''
        return self._read_var(4, self._test_min)
        
    def get_test_max(self):
        '''
        Queries UDSPT for the calibration data and return maximun of transmission.
        '''
        return self._read_var(4, self._test_max)

    def go_lock(self):
        '''
        Sets the loop into "search for lock" state. At the begining it \
        checks predcessor status and after the loop status and acts accordingly.
        '''
        if self.__predcessor is not None:
            logger.warning('Enabling automatically: ' + self.__predcessor.param.name)
            self.__predcessor.go_lock()
            
            timeout = 5
            while (self.__predcessor.check_status() != 2 or timeout == 0):
                time.sleep(1)
                timeout = timeout - 1
        
        status = self.check_status()
        if status == 0:  # Unlocked
            #self.__dev_proxy.SetGain([[4, 1],[self._button_gname]])
            self.__dev_proxy.set_gain(self._button_gname, 1, core=4)
        elif status == 1 or status == 2:
            return  # It is already lock
        elif status == 3:  # Test ramp
            # unlock
            self.go_idle()
            # wait for unlock
            time.sleep(1)
            # excecute go_lock() again
            self.go_lock()
            
        if enable_logging is True:
            logger.info(self.param.name + ' goes to LOCK')

    def go_idle(self):
        '''
        Sets the loop into idle state. At the begining it checks successor \
        status and after the loop status and acts accordingly.
        '''
        if self.__successor is not None:
            logger.warning('Disabling automatically: ' + self.__successor.param.name)
            self.__successor.go_idle()
            time.sleep(1)
        
        status = self.check_status()
        if status == 0:
            return  # It is already idle
        else:
            if self._setpoint_gname != "":
                self.set_setpoint(0.0)
            #self.__dev_proxy.SetGain([[4, 0],[self._button_gname]])
            self.__dev_proxy.set_gain(self._button_gname, 0, core=4)
            
        if enable_logging is True:
            logger.info(self.param.name + ' goes to IDLE')

    def go_ramp(self):
        '''
        Sets the loop into test state when the board generates ramp \
        for cavity alignment. At the begining it checks the status of\
         the loop and acts accordingly.
        '''
        if self.__predcessor is not None:
            logger.warning('Enabling automatically: ' + self.__predcessor.param.name)
            self.__predcessor.go_lock()
            
            timeout = 5
            while (self.__predcessor.check_status() != 2 or timeout == 0):
                time.sleep(1)
                timeout = timeout - 1
                
        status = self.check_status()
        if status == 0:
            #self.__dev_proxy.SetGain([[4, 2],[self._button_gname]])
            self.__dev_proxy.set_gain(self._button_gname, 2, core=4)
        elif status == 1 or status == 2:
            # unlock
            self.go_idle()
            # wait for unlock
            time.sleep(1)
            # excecute go_ramp() again
            self.go_ramp()
        elif status == 3:
            return  # It is already ramping
            
        if enable_logging is True:
            logger.info(self.param.name + ' goes to TEST RAMP')
            
    def dsp_load_settings(self):
        '''
        Downloads settings from DSP to param.
        '''
        self.param.ramp_freq = self.get_ramp_freq()
        self.param.ramp_ampl = self.get_ramp_ampl()
        self.param.ramp_offs = self.get_ramp_offs()
        self.param.gain = self.get_loop_gain()
        #self.param.setpoint = self.get_setpoint()
            
    def file_load_settings(self, path=""):
        '''
        Load default settings from file using pickle.
        '''
        path = '' if path is False else path
        
        filename = path + self.param.name + self._settings_filename_suffix
        
        if os.path.isfile(filename):
            logger.info("Reading from: " + str(filename))
            with open(filename, "rb") as infile:
                self.param = pickle.load(infile)
                self.param.print_settings()
        else:
            raise Exception(filename + " file does not exist!")

    def save_settings(self, path=""):
        '''
        Save current settings to file using pickle.
        '''
        filename = path + self.param.name + self._settings_filename_suffix
        logger.info("Saving to: " + str(filename))
        with open(filename, "wb") as outfile:
            pickle.dump(self.param, outfile)

    def reset_filter(self):
        '''
        Obsolete function for reseting loop IIR filter. With FSM \
        necessary resets are excecuted directly by FSM.
        '''
        logger.warning('Try not to reset filters manually!')
        self.__dev_proxy.ResetFilter([[4],[self._filter_name]])
