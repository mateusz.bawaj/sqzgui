#!/usr/bin/env python3

'''Docstring for GUI_sq module.'''

__author__ = "Mateusz Bawaj"
__copyright__ = "Copyright 2019, Virgo Project"
__license__ = "CC0"
__version__ = "1.0.0"
__maintainer__ = "Mateusz Bawaj"
__email__ = "mateusz.bawaj@pg.infn.it"
__status__ = "Production"

from PyQt5 import QtGui

from os.path import expanduser
home = expanduser("~")
settings_folder = "/.squeez_loops/"

class GUIloop:
    '''
    Doc string for GUIloop class
    onButtons - qt button object list which enable the loop
    offButtons - qt button object list which disable the loop
    rampOnButtons - qt button object list
    rampOffButtons - qt button object list
    neighbours - DSP_sq object list of loops handled by the same DSP
    '''
    color_lock = QtGui.QColor(51, 255, 51)
    color_off = QtGui.QColor(0, 102, 0)
    #self.color_unlock = QtGui.QColor(255, 0, 0)  # Old colour
    color_problem = QtGui.QColor(255, 0, 0)
    color_ramp = QtGui.QColor(255, 162, 57)
    color_unavailable = QtGui.QColor(128, 128, 128)
    #self.color_problem = QtGui.QColor(255, 255, 0)
    color_unlock = QtGui.QColor(255, 255, 0)
    
    def __init__(self, DSP_loop, onButtons, offButtons, rampOnButtons, rampOffButtons, \
                 defaultButton, gainButton, rampFreqSpin, rampAmpSpin, rampOffSpin, gainSpin, \
                 Led_controls, resetButton = None, neighbours = None, setSpin = None, transmLCD = None, \
                 errLCD = None, rampMin = 0., rampMax = 1.):
        self._DSP_loop = DSP_loop
        
        self._onButtons = onButtons
        self._offButtons = offButtons
        self._rampOnButtons = rampOnButtons
        self._rampOffButtons = rampOffButtons
        self._defaultButton = defaultButton
        self._gainButton = gainButton
        self._rampFreqSpin = rampFreqSpin
        self._rampAmpSpin = rampAmpSpin
        self._rampMin = rampMin
        self._rampOffSpin = rampOffSpin
        self._rampMax = rampMax
        self._gainSpin = gainSpin
        self._resetButton = resetButton
        self._Led_controls = Led_controls
        self._transmLCD = transmLCD
        self._errLCD = errLCD
        
        self._neighbours = neighbours
        
        self._setSpin = setSpin
        
        self.old_status = 1000
        
        # Connect callbacks
        for onButt in onButtons:
            onButt.clicked.connect(self._on)
        for offButt in offButtons:
            offButt.clicked.connect(self._off)
        for rampOnButt in rampOnButtons:
            rampOnButt.clicked.connect(self._ramp_on)
        for rampOffButt in rampOffButtons:
            rampOffButt.clicked.connect(self._ramp_off)
        defaultButton.clicked.connect(self._gainButton_callback)
        gainButton.clicked.connect(self._gain_update)
        if resetButton is not None:
            resetButton.clicked.connect(self._reset)
        if setSpin is not None:
            setSpin.valueChanged.connect(self._set_setpoint)
            
        rampAmpSpin.valueChanged.connect(self._ramp_ampl_change)
        rampOffSpin.valueChanged.connect(self._ramp_offs_change)
        
        # Correct ramp parameters at init
        self._ramp_ampl_change()
        self._ramp_offs_change()
        
    def _on(self):
        self._DSP_loop.go_lock()

    def _off(self):
        self._DSP_loop.go_idle()

    def _ramp_on(self):
        '''
        Turn on the ramp. It checks if the ramp is enabled for other \
        loops which share the same ramp generator. In case it disables \
        their ramps.
        '''
        if self._neighbours is not None:
            for nei in self._neighbours:
                if nei.check_status() == 3:
                    nei.go_idle()
                    time.sleep(0.3)
        
        self._off()
        freq = self._rampFreqSpin.value()
        amp = self._rampAmpSpin.value()
        off = self._rampOffSpin.value()
        self._DSP_loop.set_ramp_freq(freq)
        self._DSP_loop.set_ramp_ampl(amp)
        self._DSP_loop.set_ramp_offs(off)
        self._DSP_loop.go_ramp()
        
        for onButt in self._rampOnButtons:
            onButt.setEnabled(False)
            onButt.setStyleSheet("background-color: none")
            
        for offButt in self._rampOffButtons:
            offButt.setEnabled(True)
            offButt.setStyleSheet("background-color: none")

    def _ramp_off(self):
        self._DSP_loop.go_idle()
        
        for onButt in self._onButtons:
            onButt.setEnabled(True)
            onButt.setStyleSheet("background-color: none")
        
        for offButt in self._offButtons:
            offButt.setEnabled(False)
            offButt.setStyleSheet("background-color: none")
            
    def _ramp_ampl_change(self):
        '''
        This function is called "on change" of amplitude Spin.
        It assures that maximum and minimum of the ramp does not exceed 1.0
        and -1.0 correspondingly by reducing the offset.
        '''
        eps = 0.01
        if (self._rampOffSpin.value() + self._rampAmpSpin.value()) > self._rampMax-eps:
            self._rampOffSpin.setValue(self._rampMax - self._rampAmpSpin.value())
            
        if (self._rampOffSpin.value() - self._rampAmpSpin.value()) < self._rampMin+eps:
            self._rampOffSpin.setValue(self._rampMin + self._rampAmpSpin.value())
        
    def _ramp_offs_change(self):
        '''
        This function is called "on change" of offset Spin.
        It assures that maximum and minimum of the ramp does not exceed 1.0
        and -1.0 correspondingly by reducing the amplitude.
        '''
        eps = 0.01
        if (self._rampOffSpin.value() + self._rampAmpSpin.value()) > self._rampMax-eps:
            self._rampAmpSpin.setValue(self._rampMax - self._rampOffSpin.value())
            
        if (self._rampOffSpin.value() - self._rampAmpSpin.value()) < self._rampMin+eps:
            self._rampAmpSpin.setValue(-self._rampMin + self._rampOffSpin.value())
        
    def _param_to_GUI(self):
        '''
        Updates spinBoxes in the SHG tabs with the parameters values from .param.
        '''
        if self._DSP_loop.param.gain is None:
            print("Probably the gain was not correctly downloaded from SAT board")  # logger.warning not available
            self._DSP_loop.param.gain = 0.
        
        self._gainSpin.setValue(self._DSP_loop.param.gain)
        
        if self._DSP_loop.param.ramp_freq is None:
            print("Probably the gain was not correctly downloaded from SAT board")
            self._DSP_loop.param.ramp_freq = 0.
            
        self._rampFreqSpin.setValue(self._DSP_loop.param.ramp_freq)
        
        if self._DSP_loop.param.ramp_ampl is None:
            print("Probably the gain was not correctly downloaded from SAT board")
            self._DSP_loop.param.ramp_ampl = 0.
            
        self._rampAmpSpin.setValue(self._DSP_loop.param.ramp_ampl)
        
        if self._DSP_loop.param.ramp_offs is None:
            print("Probably the gain was not correctly downloaded from SAT board")
            self._DSP_loop.param.ramp_offs = 0.
            
        self._rampOffSpin.setValue(self._DSP_loop.param.ramp_offs)
        
    def _GUI_to_param(self):
        '''
        '''
        self._DSP_loop.param.gain = self._gainSpin.value()
        self._DSP_loop.param.ramp_freq = self._rampFreqSpin.value()
        self._DSP_loop.param.ramp_ampl = self._rampAmpSpin.value()
        self._DSP_loop.param.ramp_offs = self._rampOffSpin.value()
        
    def save_settings(self, path=""):
        self._GUI_to_param()
        self._DSP_loop.save_settings(path=path)


    def load_settings(self, path=""):
        '''
        Action taken after 'Set Default' button is pushed. It loads default \
        values from *.pkl file into the param structure and displays them \
        in the GUI.
        '''
        try:
            self._DSP_loop.file_load_settings(path)
        except Exception as e:
            print("Error: " + str(e))
            return 1
        else:
            self._param_to_GUI()

    def _gain_update(self):
        gain = self._gainSpin.value()
        self._DSP_loop.set_loop_gain(gain)
        
    def _gain_download(self):
        gain_from_DSP = self._DSP_loop.get_loop_gain()
        self._gainSpin.setValue(gain_from_DSP)
        
    def _gainButton_callback(self):
        gain_from_DSP = self._DSP_loop.get_loop_gain()  # Gain
        gain_from_interface = self._gainSpin.value()
        
        if gain_from_interface != gain_from_DSP:
            self._gain_download()
        else:
            self.load_settings(home + settings_folder)

    def _set_setpoint(self):  # Only for PID lock
        _max = self._DSP_loop.get_test_min()
        _min = self._DSP_loop.get_test_max()
        set_point_deviation = -(_max - _min) * self._setSpin.value() / 2.0
        if _min != 0.0:
            self._DSP_loop.set_setpoint(set_point_deviation)
        else:
            print("Min and max not found during calibration!")

    # TODO Test this function
    def _reset(self):
        '''
        Obsolete. Do not use.
        '''
        self._DSP_loop.reset_filter()
        
    def set_LED_status(self, led, status):
        if status == 1:  # calibration and lock acquisition
            led.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 29px; background-color: %s}" %  self.color_unlock.name());  # When border radius is not 1/2 of hight the LED becomes square!
        elif status == 2:  # locked
            led.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 29px; background-color: %s}" %  self.color_lock.name());
        elif status == 3:  # test ramp
            led.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 29px; background-color: %s}" %  self.color_ramp.name());
        elif status == 0:  # unlocked
            led.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 29px; background-color: %s}" %  self.color_off.name());
        elif status == 13:  # FSM in error state
            led.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 29px; background-color: %s}" %  self.color_problem.name());
        else:  # any undefined value of state
            self.led.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 29px; background-color: %s}" %  self.color_problem.name());
        
    def _update_status(self):
        _status = self._DSP_loop.check_status()  # Status
        if _status != self.old_status:
            for led in self._Led_controls:
                self.set_LED_status(led, _status)

            # 0 = Idle; ON E; OFF D; rON E; rOFF D
            # 1 = Try ; ON D; OFF E; rON D; rOFF D
            # 2 = Lock; ON D; OFF E; rON E; rOFF D
            # 3 = Ramp: ON E; OFF D; rON D; rOFF E
            #13 = Err ; ON D; OFF E; rON D; rOFF D
            
            for onButt in self._onButtons:
                if int(_status) == 0 or int(_status) == 3:
                    onButt.setEnabled(True)
                    onButt.setStyleSheet("background-color: none")
                else:
                    onButt.setEnabled(False)
                    onButt.setStyleSheet("background-color: none")
            
            for offButt in self._offButtons:
                if int(_status) == 1 or int(_status) == 2 or int(_status) == 13:
                    offButt.setEnabled(True)
                    offButt.setStyleSheet("background-color: none")
                else:
                    offButt.setEnabled(False)
                    offButt.setStyleSheet("background-color: none")
                    
            for onRampButt in self._rampOnButtons:
                if int(_status) == 0 or int(_status) == 2:
                    onRampButt.setEnabled(True)
                    onRampButt.setStyleSheet("background-color: none")
                else:
                    onRampButt.setEnabled(False)
                    onRampButt.setStyleSheet("background-color: none")
            
            for offRampButt in self._rampOffButtons:
                if int(_status) == 3:
                    offRampButt.setEnabled(True)
                    offRampButt.setStyleSheet("background-color: none")
                else:
                    offRampButt.setEnabled(False)
                    offRampButt.setStyleSheet("background-color: none")
            
            self.old_status = _status

        if self._transmLCD is not None:
            transm = self._DSP_loop.get_tran_signal()  # Transmission monitor
            self._transmLCD.display(transm)
        if self._errLCD is not None:
            err = self._DSP_loop.get_error_signal()  # Error monitor
            self._errLCD.display(err)
        
        # Gain spin
        gain_from_DSP = self._DSP_loop.get_loop_gain()  # Gain
        gain_from_interface = self._gainSpin.value()

        if gain_from_interface != gain_from_DSP:  # Change button style and function
            self._defaultButton.setStyleSheet("background-color: darkRed")
            self._defaultButton.setText("Download gain")
        else:
            self._defaultButton.setStyleSheet("background-color: ")
            self._defaultButton.setText("Set Default")
        
        # Setpoint spin
        if self._setSpin is not None:
            _max = self._DSP_loop.get_test_min()
            _min = self._DSP_loop.get_test_max()
            if _max - _min != 0.0:
                set_point_deviation = self._DSP_loop.get_setpoint()
                # See def _set_setpoint(self): function
                setpoint_from_DSP = -2.0 * set_point_deviation / (_max - _min)
            
                setpoint_from_interface = self._setSpin.value()
            
                if setpoint_from_interface != setpoint_from_DSP:
                    self._setSpin.setValue(setpoint_from_DSP)
