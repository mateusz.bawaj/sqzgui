#!/usr/bin/env python

from PyQt4 import QtGui, uic
from PyQt4 import QtCore as core
import PyTango

import sys, os
import math
import time
import getpass




class MyWindow(QtGui.QMainWindow):
    
    def __init__(self):
        super(MyWindow, self).__init__()

        user = getpass.getuser()

        uic.loadUi('/users/vardaro/Documents/Sources/GUI_tools/SAT_controls/Offset_qtGUI.ui', self) #chiama il file di qt 
        

        self.board1 = 'dspserver/p41/55'
        self.board2 = 'dspserver/p42/98'
        self.servername = 'satserver/sq/1'
        self.msg = ''
        self.devState = 'OFF'
        self.devState1 = 'OFF'


        self.dbg = True

        self.set_button.clicked.connect(self.offset_update)
        self.exit_button.clicked.connect(self.exit) 


        self.timer  = core.QTimer(self)
        self.timer.setInterval(1000)          
        self.timer.timeout.connect(self.offset_status) 

        self.actionAbout.triggered.connect(self.about_qt) #Tendina About
        self.actionExit.triggered.connect(self.exit)      #Per uscire da menu tendina
        self.timer_updt()

        #self.SHG_ramp_default()
        #self.MZ_ramp_default()

        self.show()

        self.SATboard_connect(self.board1, self.board2)
#        self.GainServer_connect(self.servername)

    def timer_updt(self):
        self.timer.start()

    def SATboard_connect(self, board, board1): #SISTEMATA

        self.squeeze1 = PyTango.DeviceProxy(board)
        properties = PyTango.Database().get_device_property(board,['serial_number'])
        serial_number = properties['serial_number'].__str__()[2:-2]
        devState=str(self.squeeze1.state())
        self.devState = devState

        self.squeeze2 = PyTango.DeviceProxy(board1)
        properties1 = PyTango.Database().get_device_property(board1,['serial_number'])
        serial_number1 = properties1['serial_number'].__str__()[2:-2]
        devState1=str(self.squeeze2.state())
        self.devState1 = devState1

        if serial_number == '41055':         
            bname = 'squeeze1'            

        if serial_number1 == '42098':         
            bname1 = 'squeeze2'

        if ((devState=="RUNNING" or devState == "ALARM") and (devState1=="RUNNING" or devState1 == "ALARM")):
            self.msg = ('DSP ' + bname + ' connected!' + '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t' + 'DSP ' + bname1 + ' connected!' )
            self.statusbar.showMessage(self.msg)
            self.dbg = False

        elif (devState=="RUNNING" and devState1!="RUNNING"):
            self.dbg = True
            self.msg = ('DSP ' + bname + ' connected!' + '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t' + 'DSP ' + bname1 + ' not found!' )
            self.statusbar.showMessage(self.msg)

        elif (devState!="RUNNING" and devState1=="RUNNING"):
            self.dbg = True
            self.msg = ('DSP ' + bname + ' not found!' + '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t' + 'DSP ' + bname1 + ' connected!' )
            self.statusbar.showMessage(self.msg)

        elif (devState!="RUNNING" and devState1!="RUNNING"):
            self.msg = ('DSP ' + bname + ' not found!' + '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t' + 'DSP ' + bname1 + ' not found!' )
            self.statusbar.showMessage(self.msg)


        if self.dbg == False:
            self.statusbar.setStyleSheet("background-color: green;")
        else: 
            self.statusbar.setStyleSheet("background-color: red;")

    def GainServer_connect(self, gain_server): 
        self.gain_server = PyTango.DeviceProxy(gain_server)
        properties = PyTango.Database().get_device_property(gain_server, ['name', 'poll_period'])
        name = properties['name'][0]
        poll_period = properties['poll_period'][0]
        #print("Poll period of the selected device: " + poll_period + " ms")
        #print "Filling attributes..."
        self.gainList_new = self.gain_server.get_attribute_list()
        #print self.gainList_new

    def offset_update(self):

        off_read = self.MCIR_trans_set.text()
        if off_read != "":
            self.squeeze1.SetGain([[4,float(off_read)],['MCIR_troff']])
            self.MCIR_trans_set.clear()

        off_read = self.MCIR_err_set.text()
        if off_read != "":
            self.squeeze1.SetGain([[4,float(off_read)],['MCIR_eroff']])
            self.MCIR_err_set.clear()

        off_read = self.SHG_trans_set.text()
        if off_read != "":
            print float(off_read)
            self.squeeze1.SetGain([[4,float(off_read)],['SHG_troff']])
            self.SHG_trans_set.clear()

        off_read = self.SHG_err_set.text()
        if off_read != "":
            self.squeeze1.SetGain([[4,float(off_read)],['SHG_eroff']])
            self.SHG_err_set.clear()

        off_read = self.MCG_trans_set.text()
        if off_read != "":
            self.squeeze1.SetGain([[4,float(off_read)],['MCG_troff']])
            self.MCG_trans_set.clear()

        off_read = self.MCG_err_set.text()
        if off_read != "":
            self.squeeze1.SetGain([[4,float(off_read)],['MCG_eroff']])
            self.MCG_err_set.clear()

        off_read = self.OPO_trans_set.text()
        if off_read != "":
            self.squeeze2.SetGain([[4,float(off_read)],['OPO_troff']])
            self.OPO_trans_set.clear()

        off_read = self.OPO_err_set.text()
        if off_read != "":
            self.squeeze2.SetGain([[4,float(off_read)],['OPO_eroff']])
            self.OPO_err_set.clear()


    def offset_status(self):

        #SHG#

        off = self.squeeze1.GetGain([[4],['MCIR_troff']]) 
        self.MCIR_trans_actual.setText(str(off))
        off = self.squeeze1.GetGain([[4],['MCIR_eroff']]) 
        self.MCIR_err_actual.setText(str(off))
        off = self.squeeze1.GetGain([[4],['SHG_troff']]) 
        self.SHG_trans_actual.setText(str(off))
        off = self.squeeze1.GetGain([[4],['SHG_eroff']]) 
        self.SHG_err_actual.setText(str(off))
        off = self.squeeze1.GetGain([[4],['MCG_troff']]) 
        self.MCG_trans_actual.setText(str(off))
        off = self.squeeze1.GetGain([[4],['MCG_eroff']]) 
        self.MCG_err_actual.setText(str(off))
        off = self.squeeze2.GetGain([[4],['OPO_troff']]) 
        self.OPO_trans_actual.setText(str(off))
        off = self.squeeze2.GetGain([[4],['OPO_eroff']]) 
        self.OPO_err_actual.setText(str(off))
   

    def about_qt(self):
        string1 = "Squeezer Controls - About"
        string2 = "Version 2.0\n\n"
        string3 = "Author: Marco Vardaro \n marco.vardaro@pd.infn.it"
        self.prova = QtGui.QMessageBox.about(self, string1, string2+string3)

    def exit(self):
        QtGui.qApp.closeAllWindows()
#        QtGui.QApplication.quit()

if __name__ == "__main__":
               
    app = QtGui.QApplication(sys.argv)
    
    #open the window
    window = MyWindow()
    
    sys.exit(app.exec_())

