#!/usr/bin/env python3

'''
GUI for R&D squeezer bench automation at 1500W lab.

Loop parameters logic:
Parameters like loop gain, ramp frequency etc. are stored in three places; \
in the DSP boards, in the corresponding widget value and in the param structure \
inside each loop object.
When the program is loaded it reads values from DSP and saves them into param \
and shows them in the GUI (setting widgets values).
Each command (Ramp ON, Set Gain, etc.) uses values stored in GUI (widget).
There are two commands to store (Save) and recall (Import) settings of all \
loops at once. They are in the file menu. Both functions operate on \
pickle files stored in the user folder and param structure.
"Save settings" copies all current values from GUI widgets and stores \
them into param structure. Then it saves each structure into a separate file.
"Import settings" performs reverse operation. It looks for setting files \
and if available it recalls values to param and updates widgets.
Any further changes in the GUI widgets does not cause modification of param. \
Settings recalled via "Import setting" can be copied to widgets at any time \
by pushing the corresponding "Set Default" button. Remember that param is \
initiated at the begginig of the program with values stored in the DSP \
and until "Import setting" function is called this parameters can be used \
to recover the settings from the beggining of the session.
'''

from PyQt5 import QtGui, QtWidgets, uic
from PyQt5 import QtCore as core

import subprocess
import sys, os
import math
import time
import getpass
import logging

from os.path import expanduser
home = expanduser("~")
settings_folder = "/.squeez_loops/"

import DSP_sq.DSP_sq as DSP_sq
from GUI_sq import GUIloop

__author__ = "Mateusz Bawaj, Marco Vardaro"
__copyright__ = "Copyright 2019, Virgo Project"
__credits__ = ["Mateusz Bawaj, Marco Vardaro, Diego Pasuello"]
__license__ = "CC0"
__version__ = "6.0.1"
__maintainer__ = "Mateusz Bawaj"
__email__ = "mateusz.bawaj@pg.infn.it"
__status__ = "Production"

#import pickle

version = 6

class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()

        user = getpass.getuser()

        uic.loadUi(os.path.dirname(os.path.realpath(__file__)) + "/Controls_qtGUI.ui", self)

        if user == "zendri":
            self.label_9.setText('Mach Zendri')
        
        # === Historically defalt parameters === Remove?
        # Will be used only in case of missing communication with DSP.
        # squeeze2
        self.SHG_default_param = DSP_sq.LoopSettings("SHG", 1.0, ramp_freq=5.0, ramp_ampl=0.5,\
                                                     ramp_offs=0.0, setpoint=0.0, enabled=False)
        self.MCG_defalut_param = DSP_sq.LoopSettings("MCG", 0.2, ramp_freq=5.0, ramp_ampl=0.5,\
                                         ramp_offs=0.0, setpoint=0.0, enabled=False)
        self.MZ_defalut_param = DSP_sq.LoopSettings("MZ", 100.0, ramp_freq=2.0, ramp_ampl=0.9,\
                                        ramp_offs=0.0, setpoint=0.0, enabled=False)
        # squeeze3
        self.MCIR_defalut_param = DSP_sq.LoopSettings("MCIR", -20.0, ramp_freq=5.0, ramp_ampl=0.5,\
                                          ramp_offs=0.0, setpoint=0.0, enabled=False)
        self.OPO_defalut_param = DSP_sq.LoopSettings("OPO", 250.0, ramp_freq=5.0, ramp_ampl=0.5,\
                                         ramp_offs=0.0, setpoint=0.0, enabled=False)
        #self.COHL_defalut_param = DSP_sq.LoopSettings("COH LO", 1.0, ramp_freq=5.0, ramp_ampl=0.5,\
        #                                 ramp_offs=0.0, setpoint=0.0, enabled=False)
        
        # squeeze1 board
        # IP: 
        # Signals gathering, dataDisplay
        #self.board1 = 'dspserver/p41/55'  # squeeze1
        self.board1 = 'squeeze1'  # squeeze1
        
        # squeeze2 board
        # IP: 172.16.2.98
        # Finite State Machines and loop filters for:
        # - SHG
        # - MCG
        # - MZ
        #self.board2 = 'dspserver/p42/98'  # squeeze2
        self.board2 = 'squeeze2'  # squeeze2
        
        # squeeze3 board
        # IP: 172.16.2.112
        # Finite State Machines and loop filters for:
        # - OPO
        # - MCIR
        # - COHL + COHP
        #self.board3 = 'dspserver/p42/112'  # squeeze3
        self.board3 = 'squeeze3'  # squeeze3
        
        self.servername = 'satserver/sq/1'
        self.msg = ''

        self.cursor_up_old = 1
        self.cursor_down_old = 1

        self.dbg = True

        self.timer = core.QTimer(self)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.control_status) 

        #self.MCGLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 30px; background-color: %s}" %  self.color_unavailable.name());
        #self.OPOLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 30px; background-color: %s}" %  self.color_unavailable.name());

        self.OPOonButton_3.clicked.connect(self.OPOsh_on)
        self.OPOoffButton_3.clicked.connect(self.OPOsh_off)
        self.OPOgainButton_2.clicked.connect(self.OPOsh_gain_update)
        self.OPOsetSpin_2.valueChanged.connect(self.OPOsh_set_setpoint)
        self.OPOresetButton_2.clicked.connect(self.OPOsh_reset)
        
        #self.CConButton.clicked.connect(self.COHL_on)
        #self.CCoffButton.clicked.connect(self.COHL_off)

        self.Squeeze1Reset.clicked.connect(self.reset_squeeze1)
        self.Squeeze2Reset.clicked.connect(self.reset_squeeze2)

        self.UdateCursorsButton.clicked.connect(self.update_cursors)

        self.actionOffset_Manager.triggered.connect(self.offset_manager)
        self.actionOPO.triggered.connect(self.OPOPLL)
        self.actionCOH.triggered.connect(self.COHPLL)
        self.actionOPO_2.triggered.connect(self.OPOPeltier)
        self.actionSHG.triggered.connect(self.SHGPeltier)
        self.actionG25squeeze3.triggered.connect(lambda: self.dds_start('squeeze3'))
        self.actionAstor.triggered.connect(self.AstorCmplsqueeze)
        self.actionAstor1.triggered.connect(self.AstorCmplsqueeze1)

        self.actionImport_settings.triggered.connect(self.loadAll)
        self.actionSave_settings.triggered.connect(self.saveAll)
        self.actionAbout.triggered.connect(self.about_qt) #Tendina About
        self.actionExit.triggered.connect(self.exit)      #Per uscire da menu tendina

        self.show()

        if self.SATboard_connect():
            print("Connecting boards failed!!!")
            #return 1 # 23.06.2021 force start even without DSP.
        
        self.timer_updt()

    def timer_updt(self):
        self.timer.start()

    def SATboard_connect(self):
        print("Connecting boards")
        # === squeeze1 (no loops on this board!!!) ===
        try:
            self.squeeze1 = DSP_sq.DSP_device(self.board1)
            if self.squeeze1._dev_state == "OFF":
                raise Exception('squeeze1 is OFF')
        except Exception as e:
            print("Error: " + str(e))
            return 1  # Replace with a better exit

        self.squeeze1.info()
        
        # === squeeze2 ===
        try:
            self.squeeze2 = DSP_sq.DSP_device(self.board2)
            if self.squeeze2._dev_state == "OFF":
                raise Exception('squeeze2 is OFF')
        except Exception as e:
            print("Error: " + str(e))
            return 1
            
        self.squeeze2.printGainList()
        self.squeeze2.info()
        
        # DSP setting
        self.SHG_loop = DSP_sq.Loop(self.squeeze2, 'SHG loop', 'SHG_on', '_$S_STATUS$_',\
            'SHG_gain', 'trian_freq', 'trian_amp1', 'SHG_roff', '_$S_ERR$_', '_$S_TRAN$_',
            test_min_varname = '_$S_MIN_TEST$_', test_max_varname = '_$S_MAX_TEST$_')
            
        self.MCG_loop = DSP_sq.Loop(self.squeeze2, 'MCG loop', 'MCG_on', '_$M_STATUS$_',\
            'MCG_gain', 'trian_freq', 'trian_amp1', 'trian_off', '_$M_ERR$_', '_$M_TRAN$_',\
            test_min_varname = '_$M_MIN_TEST$_', test_max_varname = '_$M_MAX_TEST$_', predcessor = self.SHG_loop)
        self.SHG_loop.set_successor(self.MCG_loop)  # Cannot be defined in the constructor
        
        self.MZ_loop = DSP_sq.Loop(self.squeeze2, 'MZ loop', 'MZ_on', '_$Z_STATUS$_',\
            'MZ_gain', 'trian_freq', 'trian_amp1', 'trian_off', '_$Z_ERR$_', '_$Z_TRAN$_', setpoint_gname = 'MZ_set',\
            test_min_varname = '_$Z_MIN_TEST$_', test_max_varname = '_$Z_MAX_TEST$_', predcessor = self.MCG_loop)
        self.MCG_loop.set_successor(self.MZ_loop)  # Cannot be defined in the constructor
        
        # GUI setting
        self.SHG_GUI = GUIloop(self.SHG_loop, [self.SHGonButton, self.SHGonButton_2], [self.SHGoffButton, self.SHGoffButton_2], \
                          [self.SHGrampOnButton], [self.SHGrampOffButton], self.SHGdefaultButton, \
                          self.SHGgainButton, self.SHGrampFreqSpin, self.SHGrampAmpSpin, \
                          self.SHGrampOffSpin, self.SHGgainSpin, [self.SHGLed_general, self.SHGLed_controls], self.SHGresetButton, \
                          neighbours = [self.MCG_loop, self.MZ_loop], transmLCD = self.SHGtransmLCD, \
                          errLCD = self.SHGerrLCD, rampMin = 0.1, rampMax = 5.5)
        
        self.SHG_loop.dsp_load_settings()
        #self.SHG_loop.param.print_settings()  # Debug in terminal
        self.SHG_GUI._param_to_GUI()
        
        self.MCG_GUI = GUIloop(self.MCG_loop, [self.MCGonButton, self.MCGonButton_2], [self.MCGoffButton, self.MCGoffButton_2], \
                          [self.MCGrampOnButton], [self.MCGrampOffButton], self.MCGdefaultButton, \
                          self.MCGgainButton, self.MCGrampFreqSpin, self.MCGrampAmpSpin, \
                          self.MCGrampOffSpin, self.MCGgainSpin , [self.MCGLed_general, self.MCGLed_controls], self.MCGresetButton, \
                          neighbours = [self.SHG_loop, self.MZ_loop], transmLCD = self.MCGtransmLCD, \
                          errLCD = self.MCGerrLCD)
        
        self.MCG_loop.dsp_load_settings()
        #self.MCG_loop.param.print_settings()  # Debug in terminal
        self.MCG_GUI._param_to_GUI()
        
        self.MZ_GUI = GUIloop(self.MZ_loop, [self.MZonButton, self.MZonButton_2], [self.MZoffButton, self.MZoffButton_2], \
                          [self.MZrampOnButton], [self.MZrampOffButton], self.MZdefaultButton, \
                          self.MZgainButton, self.MZrampFreqSpin, self.MZrampAmpSpin, \
                          self.MZrampOffSpin, self.MZgainSpin , [self.MZLed_general, self.MZLed_controls], self.MZresetButton, \
                          neighbours = [self.SHG_loop, self.MCG_loop], transmLCD = self.MZtransmLCD, \
                          setSpin = self.MZsetSpin)
        
        self.MZ_loop.dsp_load_settings()
        #self.MZ_loop.param.print_settings()  # Debug in terminal
        self.MZ_GUI._param_to_GUI()
        
        # === squeeze3 ===
        try:
            self.squeeze3 = DSP_sq.DSP_device(self.board3)
            if self.squeeze3._dev_state == "OFF":
                raise Exception('squeeze3 is OFF')
        except Exception as e:
            print("Error: " + str(e))
            return 1
        
        self.squeeze3.printGainList()
        self.squeeze3.info()
        
        # DSP setting
        self.OPO_loop = DSP_sq.Loop(self.squeeze3, 'OPO loop', 'OPO_on', '_$O_STATUS$_',\
            'OPO_gain', 'trianFreq1', 'trianAmp1', 'OPO_roff', '_$O_ERR$_', '_$O_TRAN$_',\
            test_min_varname = '_$O_MIN_TEST$_', test_max_varname = '_$O_MAX_TEST$_')
        
        self.MCIR_loop = DSP_sq.Loop(self.squeeze3, 'MCIR loop', 'MCIR_on', '_$M_STATUS$_',\
            'MCIR_gain', 'trianFreq1', 'trianAmp1', 'MCIR_roff', '_$M_ERR$_', '_$M_TRAN$_',\
            test_min_varname = '_$M_MIN_TEST$_', test_max_varname = '_$M_MAX_TEST$_')
        
        self.COHL_loop = DSP_sq.Loop(self.squeeze3, 'COHL loop', 'COH_LO_on', '_$CL_STATUS$_',\
            'COH_LO_gain', 'trianFreq1', 'trianAmp1', 'trianOff1', '_$CL_ERR$_', '_$CL_TRAN$_', setpoint_gname = 'COH_LO_set',\
            test_min_varname = '_$CL_MIN_TEST$_', test_max_varname = '_$CL_MAX_TEST$_')
        
        self.COHP_loop = DSP_sq.Loop(self.squeeze3, 'COHP loop', 'COH_P_on', '_$CP_STATUS$_',\
            'COH_P_gain', 'trianFreq1', 'trianAmp1', 'trianOff1', '_$CP_ERR$_', '_$CP_TRAN$_', setpoint_gname = 'COH_P_set',\
            test_min_varname = '_$CP_MIN_TEST$_', test_max_varname = '_$CP_MAX_TEST$_')
        
        # GUI setting
        self.OPO_GUI = GUIloop(self.OPO_loop, [self.OPOonButton, self.OPOonButton_2], [self.OPOoffButton, self.OPOoffButton_2], \
                          [self.OPOrampOnButton], [self.OPOrampOffButton], self.OPOdefaultButton, \
                          self.OPOgainButton, self.OPOrampFreqSpin, self.OPOrampAmpSpin, \
                          self.OPOrampOffSpin, self.OPOgainSpin , [self.OPOLed_general, self.OPOLed_controls], self.OPOresetButton, \
                          neighbours = [self.MCIR_loop, self.COHL_loop, self.COHP_loop], transmLCD = self.OPOtransmLCD, \
                          errLCD = self.OPOerrLCD, rampMin = 0.1, rampMax = 5.5)  # 5.5V max of the D2S converter
        
        self.OPO_loop.dsp_load_settings()
        #self.OPO_loop.param.print_settings()  # Debug in terminal
        self.OPO_GUI._param_to_GUI()
        
        self.MCIR_GUI = GUIloop(self.MCIR_loop, [self.MCIRonButton, self.MCIRonButton_2], [self.MCIRoffButton, self.MCIRoffButton_2], \
                          [self.MCIRrampOnButton], [self.MCIRrampOffButton], self.MCIRdefaultButton, \
                          self.MCIRgainButton, self.MCIRrampFreqSpin, self.MCIRrampAmpSpin, \
                          self.MCIRrampOffSpin, self.MCIRgainSpin , [self.MCIRLed_general, self.MCIRLed_controls], self.MCIRresetButton, \
                          neighbours = [self.OPO_loop, self.COHL_loop, self.COHP_loop], transmLCD = self.MCIRtransmLCD, \
                          errLCD = self.MCIRerrLCD)
                          
        self.MCIR_loop.dsp_load_settings()
        #self.MCIR_loop.param.print_settings()  # Debug in terminal
        self.MCIR_GUI._param_to_GUI()
        
        self.COHL_GUI = GUIloop(self.COHL_loop, [self.COHLonButton_2], [self.COHLoffButton_2], \
                          [self.COHLrampOnButton], [self.COHLrampOffButton], self.COHLdefaultButton, \
                          self.COHLgainButton, self.COHLrampFreqSpin, self.COHLrampAmpSpin, \
                          self.COHLrampOffSpin, self.COHLgainSpin, [self.COHLLed_controls], \
                          neighbours = [self.OPO_loop, self.MCIR_loop, self.COHP_loop], transmLCD = self.COHLtransmLCD, \
                          setSpin = self.COHLsetSpin)
        
        self.COHL_loop.dsp_load_settings()
        #self.COHL_loop.param.print_settings()  # Debug in terminal
        self.COHL_GUI._param_to_GUI()
        
        self.COHP_GUI = GUIloop(self.COHP_loop, [self.COHPonButton_2], [self.COHPoffButton_2], \
                          [self.COHPrampOnButton], [self.COHPrampOffButton], self.COHPdefaultButton, \
                          self.COHPgainButton, self.COHPrampFreqSpin, self.COHPrampAmpSpin, \
                          self.COHPrampOffSpin, self.COHPgainSpin , [self.COHPLed_controls, self.COHLed_general], \
                          neighbours = [self.OPO_loop, self.MCIR_loop, self.COHL_loop], transmLCD = self.COHPtransmLCD, \
                          setSpin = self.COHPsetSpin)

        self.COHP_loop.dsp_load_settings()
        #self.COHP_loop.param.print_settings()  # Debug in terminal
        self.COHP_GUI._param_to_GUI()
        
        # Updating status bar
        self.msg = 'DSP ' + self.squeeze1._board_name + ':' + self.squeeze1._dev_state + ' | '
        self.msg += 'DSP ' + self.squeeze2._board_name + ':' + self.squeeze2._dev_state + ' | '
        self.msg += 'DSP ' + self.squeeze3._board_name + ':' + self.squeeze3._dev_state
        
        self.statusbar.showMessage(self.msg)
        
        if all("RUNNING" for x in [self.squeeze1._dev_state, self.squeeze1._dev_state, self.squeeze1._dev_state]):
            self.statusbar.setStyleSheet("background-color: green;")
        else:
            self.statusbar.setStyleSheet("background-color: red;")
    
    def OPOsh_on(self):
        pass  # Shoulder lock not implemente

    def OPOsh_off(self):
        pass  # Shoulder lock not implemente

    def OPOsh_set_setpoint(self):
        pass  # Shoulder lock not implemente

    def OPOsh_gain_update(self):
        pass  # Shoulder lock not implemente

    def OPOsh_reset(self):
        pass  # Shoulder lock not implemente

    def update_cursors(self):  # Cursors are not implemented in the new version of DSP code
        pass
        '''
        up = self.CursorUpSpin.value()
        down = self.CursorDownSpin.value()
        self.squeeze1.SetGain([[4,up],['Curs_max']])
        self.squeeze1.SetGain([[4,down],['Curs_min']])
        '''
    
    def control_status(self):
        # SHG (reminder: @squeeze2)
        self.SHG_GUI._update_status()

        # MCG  (reminder: @squeeze2)
        self.MCG_GUI._update_status()

        # MZ (reminder: @squeeze2)
        self.MZ_GUI._update_status()
        
        # MCIR (reminder: @squeeze3)
        self.MCIR_GUI._update_status()

        # OPO  (reminder: @squeeze3)
        self.OPO_GUI._update_status()

        # COHL (reminder: @squeeze3)
        self.COHL_GUI._update_status()

        # COHP (reminder: @squeeze3)
        self.COHP_GUI._update_status()
        
        #CURSORS# Cursors are not implemented in the new version of DSP
        '''
        self.set_ramp_spin2( self.cursor_up_old, 'Curs_max', self.CursorUpSpin)
        self.set_ramp_spin2( self.cursor_down_old, 'Curs_min', self.CursorDownSpin)
        self.cursor_up_old = self.squeeze2.GetGain([[4],['Curs_max']]) 
        self.cursor_down_old = self.squeeze2.GetGain([[4],['Curs_min']]) 
    
        p_pol_off = self.ppolOffSpin.value()
        vis =  100.*((self.cursor_up_old - p_pol_off) - (self.cursor_down_old - p_pol_off))/((self.cursor_up_old - p_pol_off) + (self.cursor_down_old - p_pol_off))
        self.visibilityNumber.display(vis)
        '''
    def reset_squeeze1(self):  # Board reset must be reviewed with the new version
        pass
        '''
        opo_state = self.squeeze2.GetGain([[4],['OPO_on']])
        self.squeeze2.SetGain([[4,0],['OPO_on']])
        time.sleep(0.5)
        self.squeeze1.Download('/users/vardaro/Documents/Damping/MCIR_2level/MCIR')
        time.sleep(0.5)
        if opo_state == 1:
            self.squeeze2.SetGain([[4,1],['OPO_on']])
        '''

    def reset_squeeze2(self):
        pass
        # self.squeeze2.Download('/users/vardaro/Documents/Damping/OPO/OPO')

    def offset_manager(self):
        pass  # no need for offset manager in the new version
        #os.system(os.path.dirname(os.path.realpath(__file__)) + "/offset_qt.py \""  + "\" &")

    def OPOPLL(self):
        os.system(os.path.dirname(os.path.realpath(__file__)) + "/PLLTango/pll_notaurus.py OPO\""  + "\" &")

    def COHPLL(self):
        os.system(os.path.dirname(os.path.realpath(__file__)) + "/PLLTango/pll_notaurus.py COH\""  + "\" &")

    def OPOPeltier(self):
        os.system(os.path.dirname(os.path.realpath(__file__)) + "/PeltierTango/peltier.py OPO\""  + "\" &")

    def SHGPeltier(self):
        os.system(os.path.dirname(os.path.realpath(__file__)) + "/PeltierTango/peltier.py SHG\""  + "\" &")

    def dds_start(self, board_name):
        os.system("/users/vardaro/Documents/Sources/GUI_tools/DDS/dds_qt.py " + board_name +  " &")

    def AstorCmplsqueeze(self):
        subprocess.call(os.path.dirname(os.path.realpath(__file__)) + "/astor_cmplsqueeze.sh", shell=True)

    def AstorCmplsqueeze1(self):
        subprocess.call(os.path.dirname(os.path.realpath(__file__)) + "/astor_cmplsqueeze_start.sh", shell=True)

    def about_qt(self):
        string1 = "Squeezer Controls - About"
        string2 = "Version " + str(version) + "\n\n"
        string3 = "Author: Marco Vardaro \n marco.vardaro@pd.infn.it\n"
        string4 = "Author: Mateusz Bawaj \n mateusz.bawaj@pg.infn.it"
        self.prova = QtGui.QMessageBox.about(self, string1, string2+string3+string4)


    def loadAll(self):
        '''
        Loads user settings from *.pkl files for all loops and updates \
        values in the GUI.
        '''
        fpath = home + settings_folder
        # squeeze2
        self.SHG_GUI.load_settings(path=fpath)
        self.MCG_GUI.load_settings(path=fpath)
        self.MZ_GUI.load_settings(path=fpath)
        
        # squeeze3
        self.OPO_GUI.load_settings(path=fpath)
        self.MCIR_GUI.load_settings(path=fpath)
        self.COHL_GUI.load_settings(path=fpath)
        self.COHP_GUI.load_settings(path=fpath)
        
    def saveAll(self):
        '''
        Saves current user settings to param structure and saves the \
        structure to a *.pkl file.
        Current user settings are stored in the GUI widgets.
        '''
        fpath = home + settings_folder
        if not os.path.exists(fpath):
            os.makedirs(fpath)

        # squeeze2
        self.SHG_GUI.save_settings(path=fpath)
        self.MCG_GUI.save_settings(path=fpath)
        self.MZ_GUI.save_settings(path=fpath)
        # squeeze3
        self.OPO_GUI.save_settings(path=fpath)
        self.MCIR_GUI.save_settings(path=fpath)
        self.COHL_GUI.save_settings(path=fpath)
        self.COHP_GUI.save_settings(path=fpath)


    def exit(self):
        QtWidgets.qApp.closeAllWindows()
#        QtGui.QApplication.quit()


if __name__ == "__main__":
               
    app = QtWidgets.QApplication(sys.argv)
    
    #open the window
    window = MyWindow()
    
    sys.exit(app.exec_())
