killall gnuplot_x11

cond=$2

if [ "$cond" = "0" ]; then
	gnuplot << EOF
	reset
	set grid
	set term x11 persist
    set multiplot
    set size 1,0.5
    set origin 0,0.5
	plot '$1' u (\$1/3600.):2 w lp title 'CP Level'
    
    set origin 0,0
    plot '$1' u (\$1/3600.):3 w lp title 'Th Status'
    unset multiplot
EOF

fi
