#!/usr/bin/env python

import sys, os
from PyQt4 import QtGui, uic
from PyQt4 import QtCore as core
import serial
import datetime
import socket
import getpass
import subprocess

import PyTango


import time


class MyWindow(QtGui.QMainWindow):
    
    def __init__(self):
        super(MyWindow, self).__init__()
        
        self.dbg = False

        self.path = '/virgoDev/Squeezing1500/SAT_controls/PLLTango/'
        self.board_name = '/dev/pll'
        uic.loadUi( self.path + 'PLL_qtGUI_notaurus.ui', self) #chiama il file di qt 
        self.setWindowTitle(title)

        self.fast_locked = 0

        self.color_off = QtGui.QColor(0, 102, 0)    
        self.color_lock = QtGui.QColor(51, 255, 51)    
        self.color_unlock = QtGui.QColor(255, 0, 0)  
        self.color_alert = QtGui.QColor(255, 255, 0)  
        self.color_unavailable = QtGui.QColor(128, 128, 128)   

        self.empty_file = self.Filename.text()  
        self.filename_datetime = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

        self.start_time = time.time()

        self.timer  = core.QTimer(self)
        self.timer.setInterval(2117)          
#        self.timer.timeout.connect(self.tcm_connect) 

        self.timer1  = core.QTimer(self)
        self.timer1.setInterval(1000)          
        self.timer1.timeout.connect(self.sbupdt) 

        self.statusLabel = QtGui.QLabel()
        #self.statusbar.addWidget(self.statusLabel,1)


        self.LO_spinBox.valueChanged.connect(self.updateFast)
        self.RF_spinBox.valueChanged.connect(self.updateFast)
        self.CPGain_spinBox.valueChanged.connect(self.updateFast)
        self.PiezoVal_spinBox.valueChanged.connect(self.updateFast)
        self.PiezoMin_spinBox.valueChanged.connect(self.updateFast)
        self.PiezoMax_spinBox.valueChanged.connect(self.updateFast)
        self.direction.clicked.connect(self.updateFast)
        self.Lock_Button.clicked.connect(self.updateFast)
        self.LO_Button.clicked.connect(self.updateFast)
        self.RF_Button.clicked.connect(self.updateFast)

        self.Prop_spinBox.valueChanged.connect(self.updateSlow)
        self.Int_spinBox.valueChanged.connect(self.updateSlow)
        self.Der_spinBox.valueChanged.connect(self.updateSlow)
        self.ThGain_spinBox.valueChanged.connect(self.updateSlow)
        self.ThVal_spinBox.valueChanged.connect(self.updateSlow)
        self.ThMin_spinBox.valueChanged.connect(self.updateSlow)
        self.ThMax_spinBox.valueChanged.connect(self.updateSlow)
        self.Th_direction.clicked.connect(self.updateSlow)

        self.Update_Button.clicked.connect(self.sendcb)
        self.Start_Button.clicked.connect(self.startpushFast)
        self.Stop_Button.clicked.connect(self.stoppushFast)
      
        self.Update_Button_2.clicked.connect(self.sendcb)
        self.Start_Button_2.clicked.connect(self.startpushSlow)
        self.Stop_Button_2.clicked.connect(self.stoppushSlow)

        self.Browse_Button.clicked.connect(self.browseFile)
        self.Erase_Button.clicked.connect(self.changefile)
        self.Reset_Button.clicked.connect(self.resettime)
        self.Plot_Button.clicked.connect(self.pltstart)

        self.exitButton.clicked.connect(self.exit)        #Per uscire con bottone exit

        self.actionAbout.triggered.connect(self.about_qt) #Tendina About
        self.actionExit.triggered.connect(self.exit)      #Per uscire da menu tendina

        self.timer_updt()
        self.tcm_connect()
        self.readback()
        self.show()
        self.Update_Button.setEnabled(False)
        self.Update_Button_2.setEnabled(False)
        self.Update_Button.setStyleSheet("background-color: None")
        self.Update_Button_2.setStyleSheet("background-color: None")
        self.FastLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_off.name());   
        self.SlowLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_off.name());  

    def timer_updt(self):
        self.timer1.start()
        self.timer.start()

    def tcm_connect(self):
        try:
            self.pll = PyTango.DeviceProxy(board1)
            serial_address = self.pll.get_property('serial_address').__str__()[21:-3]
            status = self.pll.state()
            if str(status) == "RUNNING":
                msg = ('Board connected! (' + str(serial_address) + ')')
                self.dbg = False
                self.statusbar.showMessage(msg)
        except:
            self.dbg = True
            msg = ('Board not found!')
            self.statusbar.showMessage(msg)

        if self.dbg == False:
            self.statusbar.setStyleSheet("background-color: green;")
        else: 
            self.statusbar.setStyleSheet("background-color: red;")

    def readback(self):          

        if self.dbg==False:
           
            self.pll.SetAttr()
            self.pll.SetAttr()                                                
            self.LO_spinBox.setValue(self.pll.lo_div)
            self.RF_spinBox.setValue(self.pll.ref_div)
            b = self.pll.pfd_polarity                                           #F2 elemento 8 polarita: 0 se Neg 1 se Pos (diretto) 
            self.direction.setChecked(self.pll.pfd_polarity)
            b = self.pll.cp_state                                               #output CP: True enable, False not enable 
            if b==True:
                self.Start_Button.setEnabled(False)  
                self.Stop_Button.setEnabled(True) 
            else:  
                self.Start_Button.setEnabled(True)
                self.Stop_Button.setEnabled(False)
            self.CPGain_spinBox.setValue(self.pll.cp_gain)
            m = self.pll.mux_state                                              #multiplexer
            if m==1:
                self.Lock_Button.setChecked(True)          
            elif m==2:
                self.LO_Button.setChecked(True)
            elif m==4:
                self.RF_Button.setChecked(True) 

            self.PiezoVal_spinBox.setValue(self.pll.pzt_val)  
            self.PiezoMin_spinBox.setValue(self.pll.pzt_min)
            self.PiezoMax_spinBox.setValue(self.pll.pzt_max)            
            #self.ThVal_spinBox.setValue(self.pll.plt_val)
            self.ThMin_spinBox.setValue(self.pll.plt_min)
            self.ThMax_spinBox.setValue(self.pll.plt_max)
            
            self.Th_direction.setChecked(self.pll.pid_sign)
            self.Prop_spinBox.setValue(self.pll.pid_prop)
            self.Int_spinBox.setValue(self.pll.pid_int)
            self.Der_spinBox.setValue(self.pll.pid_der)      
            self.ThGain_spinBox.setValue(self.pll.pid_gain) 

            self.ThVal_spinBox.setValue(self.pll.out_slow)
            lp = self.pll.pid_enable
            if bool(lp)==True:    #pid acceso
                self.Start_Button_2.setEnabled(False)
                self.Stop_Button_2.setEnabled(True)
            else:           #pid spento
                self.Start_Button_2.setEnabled(True)
                self.Stop_Button_2.setEnabled(False)                       
            self.progressBar.setValue(self.pll.pfd_out)

        
    def updateFast(self):       
        self.Update_Button.setEnabled(True)
        self.Update_Button.setStyleSheet("background-color: darkRed")

    def startpushFast(self):   
        self.Start_Button.setEnabled(False)
        self.Stop_Button.setEnabled(True)
        self.updateFast()

    def stoppushFast(self):     
        self.Start_Button.setEnabled(True)
        self.Stop_Button.setEnabled(False)
        self.updateFast()
        
    def updateSlow(self):       
        self.Update_Button_2.setEnabled(True)
        self.Update_Button_2.setStyleSheet("background-color: darkRed")

    def startpushSlow(self):    
        self.Start_Button_2.setEnabled(False)
        self.Stop_Button_2.setEnabled(True)
        self.updateSlow()

    def stoppushSlow(self): 
        self.Start_Button_2.setEnabled(True)
        self.Stop_Button_2.setEnabled(False)
        self.updateSlow()    

    def sendcb(self, *args):
        if self.Update_Button.isEnabled() == True: 
            LOdiv=self.LO_spinBox.value()
            RFdiv=self.RF_spinBox.value()
            CPGain=self.CPGain_spinBox.value()
            r=0
            r|=int(self.LO_spinBox.value())<<2
            n=0x1
            n|=int(self.RF_spinBox.value())<<8
            f=0x12
            f|=(self.CPGain_spinBox.value()-1 & 0x7)<<15
            f|=(self.CPGain_spinBox.value()-1 & 0x7)<<18
            f&=~0x70
            if self.Lock_Button.isChecked():
                m=1<<4
            elif self.LO_Button.isChecked():
                m=2<<4
            elif self.RF_Button.isChecked():
                m=4<<4
            else:
                m=0
            f|=m                                                                #qui aggiungo il muxout sui bit M1 M2 M3

            loop_dir = self.direction.checkState()
            if loop_dir == 0:
                f|=0x80                                                         #aggiungo la polarita' a f, che e' sul bit F2 
            elif loop_dir == 2:
                f&=~0x80
            if not self.Start_Button.isEnabled() and self.Stop_Button.isEnabled():
		print 'loop_on'
                lk=0
                f&=~0x100
            else:
		print 'loop_off'
                lk=1
                f|=0x100

            if self.Start_Button.isEnabled() == False and self.Stop_Button.isEnabled() == True: 
                loop_status = 1
            else:
                loop_status = 0
            
            v=self.PiezoVal_spinBox.value()
            ll=self.PiezoMin_spinBox.value()
            ul=self.PiezoMax_spinBox.value()
            cmd0=('PZ%d,%d,%d' % (v,ll,ul))
            cmd1=('RE0x%X,0x%X,0x%X' % (r,n,f))
            cmd2=('LK1') if self.Stop_Button.isEnabled() else ('LK0')
            msg='Fast updated'
            self.pll.SendCommand(cmd0)
            print(cmd0)
            self.pll.SendCommand(cmd1)
            print(cmd1)
            self.pll.SendCommand(cmd2)
            print(cmd2)

            self.logfileFast(LOdiv, RFdiv, CPGain, loop_dir, loop_status, v, ll, ul)
            self.statusbar.showMessage(msg)


        if self.Update_Button_2.isEnabled() == True:                            
            v=self.ThVal_spinBox.value()                                      
            ll=self.ThMin_spinBox.value()    
            ul=self.ThMax_spinBox.value()
            cmd0=('TH%d,%d,%d' % (v,ll,ul))        
            #msg='DACs updated'
            
            if self.Start_Button_2.isEnabled() == False and self.Stop_Button_2.isEnabled() == True: 
                loop_status = 1
            else:
                loop_status = 0
            
            pg=self.Prop_spinBox.value()                                        #...e dati del PID
            ig=self.Int_spinBox.value()
            dg=self.Der_spinBox.value()
            m=self.ThGain_spinBox.value()
            m1 = m
            m = 10 - m/6                                                        #Gain vuole da 0 (60dB) a 20 (-60dB)
            loop_dir = self.Th_direction.checkState()                        
            if self.Th_direction.isChecked():                                   #se loop negativo
                cmd1=('PG%d,%d,%d,%d' % (-pg,-ig,-dg,m))        
            else:
                cmd1=('PG%d,%d,%d,%d' % (pg,ig,dg,m))
            if self.Start_Button_2.isEnabled() == False:
                cmd2=('FB1')        
            else:
                cmd2=('FB0')
            msg='Slow updated'
        
            self.pll.SendCommand(cmd0)
            print(cmd0)
            self.pll.SendCommand(cmd1)
            print(cmd1)
            self.pll.SendCommand(cmd2)
            print(cmd2)

            self.logfileSlow(v, ll, ul, loop_status, loop_dir, pg, ig, dg, m1)    
            self.statusbar.showMessage(msg)

        if self.Update_Button.isEnabled():
            self.Update_Button.setEnabled(False)
            self.Update_Button.setStyleSheet("background-color: none")
        if self.Update_Button_2.isEnabled():
            self.Update_Button_2.setEnabled(False)
            self.Update_Button_2.setStyleSheet("background-color: none")
        
    def sbupdt(self):

        self.tcm_connect()

        if self.dbg == False:
            self.statusbar.setStyleSheet("background-color: green;")
        else: 
            self.statusbar.setStyleSheet("background-color: red;")

        if self.dbg==False:
            flag = False
            time.sleep(0.1)

            ThMin = self.pll.plt_min
            ThMax = self.pll.plt_max
            ThDelta = ThMax-ThMin
            self.Th_progressBar.setMinimum(ThMin)
            self.Th_progressBar.setMaximum(ThMax)

            time.sleep(0.1)

            fr = self.pll.pfd_out
            ThVal = self.pll.out_slow

            self.progressBar.setValue(fr)   #aggiorno la progress bar FAST
            #self.ManoMeter.setValue(fr)
            self.Th_progressBar.setValue(ThVal)
            ThValpc= float(ThVal/65535.)*100.#valore in percentuale da mettere nel manometro
            #self.Th_ManoMeter.setValue(ThValpc)

            if flag==False:             #Entra nel seguito se non ci sono stati problemi con la comunicazione seriale
                save_en = self.Save_checkBox.checkState()
                if save_en==2:
                    outfilename = self.Filename.text() 
                    if outfilename == self.empty_file:
                        a=QtGui.QMessageBox.critical(None,'Error!',"Insert filename before starting acquisition!!!", QtGui.QMessageBox.Ok)
                        self.Save_checkBox.setCheckState(0)
                    else:
                        f = open(outfilename, "a")
                        f.write(str(time.time()-self.start_time)+'\t')
                        f.write(str(fr) + '\t' + str(ThVal) + '\t') 
                        f.write('\n')
                        f.close()
                
                if self.Stop_Button.isEnabled():  #LED del Fast
                    #self.taurusLed.setLedStatus(True)
                    if fr>200 and fr<800:
                        self.FastLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_lock.name());
                        self.fast_locked = 1   
                    elif fr<200 and fr>100:
                        self.FastLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_alert.name()); 
                        self.fast_locked = 0     
                    elif fr>800 and fr<900:
                        self.FastLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_alert.name());   
                        self.fast_locked = 0   
                    else:
                        self.FastLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_unlock.name());
                        self.fast_locked = 0      
                else:
                    self.FastLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_off.name());
        
                if self.Stop_Button_2.isEnabled():    #LED dello Slow

                    if ThValpc>5 and ThValpc<95 and self.fast_locked == 1:  #Voglio che questo led sia collegato al loop fast
                        self.SlowLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_lock.name());
                    elif ThValpc>1 and ThValpc<5 and self.fast_locked == 1:
                        self.SlowLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_alert.name()); 
                    elif ThValpc>95 and ThValpc<99 and self.fast_locked == 1:
                        self.SlowLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_alert.name()); 
                    else:
                        self.SlowLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_unlock.name());
                else:
                    self.SlowLed.setStyleSheet(".QFrame{border: 1px solid black; border-radius: 35px; background-color: %s}" %  self.color_off.name());
        

        return True

    def browseFile(self):
        self.directory = QtGui.QFileDialog.getExistingDirectory(self,"Choose a directory")
        if self.directory == self.empty_file:
            self.Filename.setText(self.empty_file)
        else:
            self.Filename.setText(self.directory+'/'+self.filename_datetime+'.dat')
   

    def changefile(self, *args):
            save_en = self.Save_checkBox.checkState()
            if save_en==2:
               self.Save_checkBox.setCheckState(0)
            if  self.Filename.text() == self.empty_file:
                a=QtGui.QMessageBox.warning(None,'Error!',"Choose folder before changing filename", QtGui.QMessageBox.Ok)
            else:
                self.filename_datetime = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
                self.Filename.setText(self.directory+'/'+self.filename_datetime+'.dat')

    def resettime(self, *args):
            self.start_time = time.time()

    def pltstart(self, *args):      #Funzione per plot
            outfilename = self.Filename.text()  
            subprocess.Popen(self.path + 'plot.sh %s 0' % (str(outfilename)), shell=True)

    def logfileFast(self, LOdiv, RFdiv, CPGain, loop_dir, loop_status, v, ll, ul):
        date = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        hostname = socket.gethostname()
        user = getpass.getuser()
        save_en = self.Save_checkBox.checkState()
        filename=self.Filename.text()
        f = open(self.path + 'PllBoard_update.log', "a")
        f.write('\n-----------------------------------------------------------------------------------\n')
        f.write('@ ' + str(date) + ' Fast Updated\t from host ' + str(hostname) + ' by user ' + str(user) + '\n')
        f.write('\t LOdiv=' + str(LOdiv) + ' RFdiv=' + str(RFdiv) + ' CPGain=' + str(CPGain) + ' loop direction: ' + str(loop_dir) + ' Loop status: ' + str(loop_status) + '\n')
        f.write('\t PZTOffset= ' + str(v) + ' PZTMin= ' + str(ll) + ' PZTMax= ' + str(ul) + '\n')
        f.write('\t Data Acquisition: ' +  str(save_en) + '\t Output filename: ' + str(filename) + '\n' )
        f.close()

    def logfileSlow(self, v, ll, ul, loop_status, loop_dir, pg, ig, dg, m1):
        date = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        hostname = socket.gethostname()
        user = getpass.getuser()
        save_en = self.Save_checkBox.checkState()
        filename=self.Filename.text()
        f = open(self.path + 'PllBoard_update.log', "a")
        f.write('\n-----------------------------------------------------------------------------------\n')
        f.write('@ ' + str(date) + ' Slow Updated\t from host ' + str(hostname) + ' by user ' + str(user) + '\n')
        f.write('\t P=' + str(pg) + ' I=' + str(ig) + ' D=' + str(dg) + ' loop direction: ' + str(loop_dir) + ' Loop status: ' + str(loop_status) + '\n')
        f.write('\t ThOffset= ' + str(v) + ' ThMin= ' + str(ll) + ' ThMax= ' + str(ul) + '\n')
        f.write('\t Data Acquisition: ' +  str(save_en) + '\t Output filename: ' + str(filename) + '\n' )
        f.close()

    def about_qt(self):
        string1 = "PLL Drive Interface - About"
        string2 = "Version 1.0\n\n"
        string3 = "Author: Marco Vardaro & Tommaso Comellato\n marco.vardaro@pd.infn.it\n tommaso.comellato@pd.infn.it"
        self.prova = QtGui.QMessageBox.about(self, string1, string2+string3)

    def exit(self):
        QtGui.qApp.closeAllWindows()

if __name__ == "__main__":

    if (len(sys.argv) == 2):
        if (sys.argv[1] == 'OPO' or sys.argv[1] == 'opo' or sys.argv[1] == 'Opo'):
            board1 = 'tango://cmplsqueeze:20000/virgopll/aux2/1'
            title = 'PLL Aux2 Laser (OPO)'
            app = QtGui.QApplication(sys.argv)
            window = MyWindow()    
            sys.exit(app.exec_())

        elif (sys.argv[1] == 'COH' or sys.argv[1] == 'coh' or sys.argv[1] == 'Coh'):
            board1 = 'tango://cmplsqueeze:20000/virgopll/aux1/1'
            title = 'PLL Aux1 Laser (COH)'
            app = QtGui.QApplication(sys.argv)
            window = MyWindow()    
            sys.exit(app.exec_())

        elif (sys.argv[1] == 'AEI_int' or sys.argv[1] == 'aei_int' or sys.argv[1] == 'Aei_int'):
#            board1 = 'tango://cmplsqueeze:20000/virgopll/coh1/2'
            board1 = 'tango://olserver129:10000/virgopll/coh1/2'
            title = 'PLL INT AEI squeezer'
            app = QtGui.QApplication(sys.argv)
            window = MyWindow()    
            sys.exit(app.exec_())

        elif (sys.argv[1] == 'AEI_ext' or sys.argv[1] == 'aei_ext' or sys.argv[1] == 'Aei_ext'):
#            board1 = 'tango://cmplsqueeze:20000/virgopll/coh2/2'
            board1 = 'tango://olserver129:10000/virgopll/coh2/2'
            title = 'PLL EXT AEI squeezer'
            app = QtGui.QApplication(sys.argv)
            window = MyWindow()    
            sys.exit(app.exec_())

        else: 
            print 'You have to specify the server: OPO or AEI_int or AEI_ext or COH'
    else:                    
        print 'You have to specify the server: OPO or AEI_int or AEI_ext or COH'
               
#    app = QtGui.QApplication(sys.argv)
    
    #open the window
#    window = MyWindow()
    
#    sys.exit(app.exec_())

