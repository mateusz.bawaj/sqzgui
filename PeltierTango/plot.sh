killall gnuplot_x11

cond=$2

if [ "$cond" = "0" ]; then
	gnuplot << EOF
	reset
	set grid
	set term x11 persist
	plot '$1' u 1:2 w l title 'Board A', '' u 1:4 w l title 'Board B' 
EOF

fi

if [ "$cond" = "1" ]; then
	gnuplot << EOF
	reset
	set grid
	set term x11 persist
	plot '$1' u 1:2 w l title 'Board A'
EOF

fi

if [ "$cond" = "2" ]; then
	gnuplot << EOF
	reset
	set grid
	set term x11 persist
	plot '$1' u 1:2 w l title 'Board B'
EOF

fi
