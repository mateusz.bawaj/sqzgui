#!/usr/bin/env python

import sys, os
from PyQt4 import QtGui, uic
from PyQt4 import QtCore as core
import PyTango

import datetime
import socket
import getpass
import subprocess


import time


class MyWindow(QtGui.QMainWindow):
    dbgA = False
    dbgB = False
    
    def __init__(self):
        super(MyWindow, self).__init__()

        #self.path = '/users/vardaro/Documents/Sources/GUI_tools/SAT_controls/PeltierTango/'
        self.path = '/virgoDev/Squeezing1500/SAT_controls/PeltierTango/'
        uic.loadUi(self.path + 'peltier_gui.ui', self) #chiama il file di qt 



        self.setWindowTitle(title)
        self.empty_file = self.filename.text()  
        self.filename_datetime = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

        self.start_time = time.time()

        self.timer  = core.QTimer(self)
        self.timer.setInterval(2000)          
        self.timer.timeout.connect(self.tcm_connect) 

        self.timer1  = core.QTimer(self)
        self.timer1.setInterval(1000)          
        self.timer1.timeout.connect(self.sbupdt) 


        self.piddirection = 0

        self.statusLabelA = QtGui.QLabel()
        self.statusbar.addWidget(self.statusLabelA,1)
        self.statusLabelB = QtGui.QLabel()
        self.statusbar.addWidget(self.statusLabelB,1)

        self.kpBox.valueChanged.connect(self.updateA)
        self.kdBox.valueChanged.connect(self.updateA)
        self.kiBox.valueChanged.connect(self.updateA)
        self.setBox.valueChanged.connect(self.updateA)
        #self.direction.clicked.connect(self.updateA)

#        self.kpBox_2.valueChanged.connect(self.updateB)
#        self.kdBox_2.valueChanged.connect(self.updateB)
#        self.kiBox_2.valueChanged.connect(self.updateB)
#        self.setBox_2.valueChanged.connect(self.updateB)
#        #self.direction_2.clicked.connect(self.updateB)

        self.updateButton.clicked.connect(self.sendcb)
        self.saveButton.clicked.connect(self.saveeepA)
        self.startButton.clicked.connect(self.startpushA)
        self.stopButton.clicked.connect(self.stoppushA)
        
#        self.updateButton_2.clicked.connect(self.sendcb)
#        self.saveButton_2.clicked.connect(self.saveeepB)
#        self.startButton_2.clicked.connect(self.startpushB)
#        self.stopButton_2.clicked.connect(self.stoppushB)

        self.browseButton.clicked.connect(self.browseFile)
        self.eraseButton.clicked.connect(self.changefile)
        self.resetButton.clicked.connect(self.resettime)
        self.plotButton.clicked.connect(self.pltstart)


#        btn = QtGui.QPushButton('Yes', self)     
        self.exitButton.clicked.connect(self.exit)

        self.action_About.triggered.connect(self.about_qt)

        self.timer_updt()
        self.tcm_connect()
#        time.sleep(5)
        self.show()
        self.readback()
        self.updateButton.setEnabled(False)
#        self.updateButton_2.setEnabled(False)
        self.updateButton.setStyleSheet("background-color: lightGrey")
#        self.updateButton_2.setStyleSheet("background-color: lightGrey")

    def timer_updt(self):
        self.timer1.start()
        self.timer.start()


    def tcm_connect(self):
        try:
            self.peltierA = PyTango.DeviceProxy(board1)
            serial_address = self.peltierA.get_property('serial_address').__str__()[21:-3]
            status = self.peltierA.state()

            if str(status) == "RUNNING":
                msgA = ('Board A connected! (' + str(serial_address) + ')')
                self.dbgA = False
            else:
                self.dbgA = True
                msgA = ('Big shit!')
        except:
            self.dbgA = True
            msgA = ('Board A not found!')
        try:    
            self.peltierB = PyTango.DeviceProxy(board2)
            serial_address = self.peltierB.get_property('serial_address').__str__()[21:-3]
            status = self.peltierB.state()
            if str(status) == "RUNNING":
                msgB = ('Board B connected! (' + str(serial_address) + ')')
                self.statusbar.showMessage(msgA + '\t\t\t\t\t\t\t\t\t\t\t\t' + msgB )
                if self.dbgA == False:
                    self.statusbar.setStyleSheet("background-color: green;")
                else: 
                    self.statusbar.setStyleSheet("background-color: yellow;")
                self.dbgB = False

        except:
            self.dbgB = True
            msgB = ('Board B not found!')                

            if self.dbgA == False:
                self.statusbar.setStyleSheet("background-color: yellow;")
            else: 
                self.statusbar.setStyleSheet("background-color: red;")
            self.statusbar.showMessage(msgA + '\t\t\t\t\t\t\t\t\t\t\t\t' + msgB )
            print (msgA + ' ' + msgB)
            self.exit()

    def myrd(self,ser,cmd):
        ser.write(cmd)
        ans=ser.readline()
        if len(ans)>0:
           ans=ans[:-1]
           print(ans)
           return ans.split(':')
        else:
           print('rd failed')
           return []

    def readback(self):

        if self.dbgA==False:
                temp_set = self.peltierA.tempset
                self.setBox.setValue(temp_set)
                temp = self.peltierA.temp
                self.tempBox.setText(str(temp))
                kp = self.peltierA.kp
                self.kpBox.setValue(kp)
                ki = self.peltierA.ki
                self.kiBox.setValue(ki)
                kd = self.peltierA.kd
                self.kdBox.setValue(kd)
                #loop_dir = int(float(ret[13]))
                loop_dir = self.piddirection
                b=True if loop_dir else False 
                #self.direction.setCheckState(b)
                #self.direction.setTristate(False)
                loop_status = self.peltierA.loopen
                b=True if loop_status else False         
                if b==True:
                    self.startButton.setEnabled(False)
                    self.stopButton.setEnabled(True) 
                else:
                    self.startButton.setEnabled(True)
                    self.stopButton.setEnabled(False)                                                                 

        
    def updateA(self):
        self.updateButton.setEnabled(True)
        self.updateButton.setStyleSheet("background-color: darkRed")

    def updateB(self):
        self.updateButton_2.setEnabled(True)
        self.updateButton_2.setStyleSheet("background-color: darkRed")


    def startpushA(self):
        self.startButton.setEnabled(False)
        self.stopButton.setEnabled(True)
        self.updateA()

    def stoppushA(self):
        self.startButton.setEnabled(True)
        self.stopButton.setEnabled(False)
        self.updateA()

    def startpushB(self):
        self.startButton_2.setEnabled(False)
        self.stopButton_2.setEnabled(True)
        self.updateB()

    def stoppushB(self):
        self.startButton_2.setEnabled(True)
        self.stopButton_2.setEnabled(False)
        self.updateB()

    def sendcb(self, *args):

       if self.updateButton.isEnabled() == True:
            kp=self.kpBox.value()
            ki=self.kiBox.value()
            kd=self.kdBox.value()
            temp_set=self.setBox.value()
            temp=float(self.tempBox.text())
            loop_dir = self.piddirection
            #loop_dir = self.direction.checkState()
            if self.startButton.isEnabled() == False and self.stopButton.isEnabled() == True: 
                loop_status = 1
            else:
                loop_status = 0

            temp_set_true = 1000*(temp_set)        
            cmd1=("set:%d" % (temp_set_true))
            cmd2=('Kp:%d' % (kp))
            cmd3=('Ki:%d' % (ki))
            cmd4=('Kd:%d' % (kd))
            cmd5=('loop:dir') if loop_dir else ('loop:rev\n')     
            cmd6=('tec:on') if loop_status else ('tec:off\n')             
            msgA='BOARD A state updated'
            if self.dbgA:
                print(cmd1)
                print(cmd2)
                print(cmd3)
                print(cmd4)
                print(cmd5)
                print(cmd6)
                print(msgA)
            else:
                self.peltierA.SendCommand(cmd1)
                self.peltierA.SendCommand(cmd2)
                self.peltierA.SendCommand(cmd3)
                self.peltierA.SendCommand(cmd4)
                self.peltierA.SendCommand(cmd5)
                self.peltierA.SendCommand(cmd6)

                print(msgA)
                self.statusbar.showMessage(msgA)
            self.logfile('A', kp, ki, kd, temp_set, loop_dir, loop_status)
            self.updateButton.setEnabled(False)
            self.updateButton.setStyleSheet("background-color: lightGrey")

    def sbupdt(self):
        if self.dbgA==False:

            tempA = self.peltierA.temp
            temp_setA = self.peltierA.tempset
            self.tempBox.setText(str(tempA))

        if self.dbgB==False:


            tempB = self.peltierB.temp
            temp_setB = self.peltierB.tempset
            self.tempBox_3.setText(str(tempB))  

        save_en = self.saveenCheck.checkState()
        if save_en==2:
            if self.dbgA==False or self.dbgB==False:
                outfilename = self.filename.text() 
                if outfilename == self.empty_file:
                    a=QtGui.QMessageBox.critical(None,'Error!',"Insert filename before to start the acqusition!!!", QtGui.QMessageBox.Ok)
                    self.saveenCheck.setCheckState(0)
                else:
                    f = open(outfilename, "a")
                    f.write(str(time.time()-self.start_time)+'\t')
                    if self.dbgA==False:
                        f.write(str(tempA) + '\t' + str(temp_setA) + '\t') 
                        if self.dbgB == True:
                            f.write('\tnan\tnan\t')              
                    if self.dbgB==False:
                        if self.dbgA==True:
                            f.write('nan\tnan\t') 
                        f.write(str(tempB) + '\t' + str(temp_setB) + '\t')
                    f.write('\n')
                    f.close()
      
        return True
   
    def saveeepA(self, *args):
        cmd='save'
        if self.dbgA:
            print(cmd)
        else:
            self.peltierA.SendCommand(cmd)	

    def browseFile(self):
        self.directory = QtGui.QFileDialog.getExistingDirectory(self,"Pick a folder")
        if self.directory == self.empty_file:
            self.filename.setText(self.empty_file)
        else:
            self.filename.setText(self.directory+'/'+self.filename_datetime+'.dat')

    def changefile(self, *args):
            save_en = self.saveenCheck.checkState()
            if save_en==2:
               self.saveenCheck.setCheckState(0)
            if  self.filename.text() == self.empty_file:
                a=QtGui.QMessageBox.warning(None,'Error!',"Chose folder before change filename", QtGui.QMessageBox.Ok)
            else:
                self.filename_datetime = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
                self.filename.setText(self.directory+'/'+self.filename_datetime+'.dat')


    def resettime(self, *args):
            self.start_time = time.time()

    def pltstart(self, *args):
            outfilename = self.filename.text()  
            subprocess.Popen(self.path + 'plot.sh %s 0' % (str(outfilename)), shell=True)

    def logfile(self, board, kp, ki, kd, temp_set, loop_dir, loop_status):
        date = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        hostname = socket.gethostname()
        user = getpass.getuser()
        save_en = self.saveenCheck.checkState()
        filename=self.filename.text()
        f = open(self.path + 'pid_update.log', "a")
        f.write('\n-----------------------------------------------------------------------------------\n')
        f.write('@ ' + str(date) + '\tBoard ' + str(board) + ' Updated\t from host ' + str(hostname) + ' by user ' + str(user) + '\n')
        f.write('\t kp=' + str(kp) + ' ki=' + str(ki) + ' kd=' + str(kd) + ' loop direction: ' + str(loop_dir) + ' PID status: ' + str(loop_status) + '\n')
        f.write('\t Data Acquisition: ' +  str(save_en) + '\t Output filename: ' + str(filename) + '\n' )
        f.close()

    def about_qt(self):
        string1 = "Peltier Driver Interface - About"
        string2 = "Version 2.0\n\n"
        string3 = "Author: Marco Vardaro\n marco.vardaro@pd.infn.it"
        self.prova = QtGui.QMessageBox.about(self, string1, string2+string3)

    def exit(self):
        QtGui.qApp.closeAllWindows()
#        QtGui.QApplication.quit()

if __name__ == "__main__":

    if (len(sys.argv) == 2):
        if (sys.argv[1] == 'OPO' or sys.argv[1] == 'opo' or sys.argv[1] == 'Opo'):
            board1 = 'tango://cmplsqueeze:20000/tcm1.0/opo/1'
            board2 = 'tango://cmplsqueeze:20000/tcm1.0/opo/2'
            title = 'Peltier Driver OPO'
            app = QtGui.QApplication(sys.argv)
            window = MyWindow()    
            sys.exit(app.exec_())

        elif (sys.argv[1] == 'SHG' or sys.argv[1] == 'shg' or sys.argv[1] == 'Shg'): 
            board1 = 'tango://cmplsqueeze:20000/tcm1.0/shg/1'
            board2 = 'tango://cmplsqueeze:20000/tcm1.0/shg/2'
            title = 'Peltier Driver SHG'
            app = QtGui.QApplication(sys.argv)
            window = MyWindow()    
            sys.exit(app.exec_())
        else: 
            print 'You have to specify the server: OPO or SHG'
    else:                    
        print 'You have to specify the server: OPO or SHG'

#    app = QtGui.QApplication(sys.argv)
    #open the window
#    window = MyWindow()
    
#    sys.exit(app.exec_())

